<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends MY_Model 
{	
	public function getLoginData($usr, $psw) 
	{
		$u = $usr; 
		$p = md5($psw);
		
		$q_cek_login = $this->db->query('SELECT * FROM users WHERE usr_username ="'.$u.'" AND usr_password ="'.$p.'"'); 
		if(count($q_cek_login->result())>0)
		{
			foreach ($q_cek_login->result() as $qck)
			{			
				if($qck->usr_security_profile_id_fk=='1' && $qck->usr_active == '1' )
				{
					$q_ambil_data = $this->db->get_where('users', array('usr_username' => $u, 'usr_security_profile_id_fk' => '1'));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 	= 'yes';
						$sess_data['usr_username'] 	= $qad->usr_username;
						$sess_data['usr_first_name'] 	= $qad->usr_first_name;
						$sess_data['usr_last_name'] 	= $qad->usr_last_name;
						$sess_data['usr_email_address'] 		= $qad->usr_email_address;
						$sess_data['users_password'] 	= $qad->usr_password;
						$sess_data['stts'] 			 	= 'Administrator';
						$sess_data['kontroller'] 	 	= 'admin';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/admin/bg_booking?booking=1');
				}
				else if($qck->usr_security_profile_id_fk=='2' && $qck->usr_active == '1' )
				{
					$q_ambil_data = $this->db->get_where('users', array('usr_username' => $u, 'usr_security_profile_id_fk' => '2'));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 	= 'yes';
						$sess_data['usr_username'] 	= $qad->usr_username;
						$sess_data['usr_first_name'] 	= $qad->usr_first_name;
						$sess_data['usr_last_name'] 	= $qad->usr_last_name;
						$sess_data['usr_email_address'] 		= $qad->usr_email_address;
						$sess_data['users_password'] 	= $qad->usr_password;
						$sess_data['stts'] 			 	= 'Sales';
						$sess_data['kontroller'] 	 	= 'sales';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/sales/bg_booking?booking=1');
				}
				else 
				{
					header('location:'.base_url().'index.php/welcome');
					$this->session->set_flashdata('info','<div class="alert alert-danger">
						                                    <button data-dismiss="alert" class="close">
						                                        &times;
						                                    </button>
						                                    <i class="fa fa-times-circle"></i>
						                                    <strong>Sorry!</strong> It seems you are having trouble Signing In.
						                                </div>');

					$this->session->set_flashdata("info2","<script type='text/javascript'>
													     setTimeout(function () { 
													     swal({
													                title: 'Oops!',
													                text:  'Your account has been deactivated',
													                type: 'warning',
													                timer: 3000,
													                showConfirmButton: true
													            });  
													     },10);  
													    </script>
													    ");
				}

				/*
				else if($qck->stts=='Direktur')
				{
					$q_ambil_data = $this->db->get_where('tbl_direktur', array('dir_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->dir_username;
						$sess_data['nama'] 		 	 = $qad->dir_nama_lengkap;
						$sess_data['stts'] 			 = 'Direktur';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/direktur');
				} */
			}
		}
		else
			{
				header('location:'.base_url().'index.php/welcome');
				$this->session->set_flashdata('info','<div class="alert alert-danger">
					                                    <button data-dismiss="alert" class="close">
					                                        &times;
					                                    </button>
					                                    <i class="fa fa-times-circle"></i>
					                                    <strong>Sorry!</strong> It seems you are having trouble Signing In.
					                                </div>');

				$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Oops!',
												                text:  'Incorrect username or password',
												                type: 'warning',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
			}

		//$this->db->close();
	}

	public function updateDataWhere($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
		return;
		$this->db->close();
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
		return;
		$this->db->close();
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get2WhereCOUNTOneItem($where,$field,$table, $count, $field2, $where2, $field3, $where3)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(".$count.") AS TOTAL FROM ".$table." WHERE ".$field."='".$where."' AND ".$field2." = '".$where2."' AND ".$field3." = '".$where3."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get1WhereCOUNTOneItem($where,$field,$table, $count, $field2, $where2)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(".$count.") AS TOTAL FROM ".$table." WHERE ".$field."='".$where."' AND ".$field2." = '".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get2WhereOneItemOrder($where,$field,$where2,$field2,$table,$fieldOrder,$order)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->order_by($fieldOrder, $order);
		$Q = $this->db->get($table);
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getAllData($table)
	{
		return $this->db->get($table);
		//return $this->db->get($table)->result();

		$this->db->close();
	}

	public function getAllData_ajax($table)
	{
		return $this->db->get($table)->result();;
		//return $this->db->get($table)->result();

		$this->db->close();
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
		return;
		$this->db->close();
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereAllItem_groupby($where,$field,$table,$group)
	{
		$this->db->group_by($group);
		$this->db->where($field,$where);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereAllItem_ajax($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table)->result();
		$this->db->close();
	}

	public function getWhereLimit($where,$field,$table,$limit)
	{
		$this->db->where($field,$where);
		$this->db->limit($limit);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereAllItemGroup($where,$field,$table,$group)
	{
		$this->db->where($field,$where);
		$this->db->group_by($group);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereAllItemOrder($where,$field,$table,$field2,$order)
	{
		$this->db->where($field,$where);
		$this->db->order_by($field2, $order);
		return $this->db->get($table);
		$this->db->close();
	}

	public function get2WhereAllItemOrder($where,$field,$where2,$field2,$table,$fieldOrder,$order)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->order_by($fieldOrder, $order);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getAllJoin($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}

	public function getAllJoin_ajax($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query->result();
		$this->db->close();
	}

	public function getWhereAllJoin($idTabel1,$idTabel2,$table1,$table2,$field1,$where1)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}

	public function getWhereAllJoin_ajax($idTabel1,$idTabel2,$table1,$table2,$field1,$where1)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$query = $this->db->get();
		return $query->result();
		$this->db->close();
	}

	public function getWhereAll2JoinGroup($idTabel1,$idTabel2,$table1,$table2,$field1,$where1,$group)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$this->db->group_by($group);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}


	public function getMaxOneData($maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function getMaxOneDataWhere($field,$where,$maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = '".$where."' ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function getMaxOneData2Where($field,$where,$field2,$where2,$maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = '".$where."' AND ".$field2." = '".$where2."' ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function get_join_all_2where_or($tbl1, $tbl2, $kd_tbl1, $kd_tbl2, $field1, $where1, $field2, $where2)
	{
		return $this->db->query("SELECT * FROM ".$tbl1." INNER JOIN ".$tbl2." ON ".$tbl2.".".$kd_tbl2." = ".$tbl1.".".$kd_tbl1." WHERE ".$field1." = '".$where1."' OR ".$field2." = '".$where2."'");
		$this->db->close();
	}

	public function get_o_2where($tbl, $field1, $where1, $field2, $where2)
	{
		return $this->db->query("SELECT * FROM ".$tbl." WHERE ".$field1." = '".$where1."' OR ".$field2." = '".$where2."' ");
		$this->db->close();
	}

	public function cek_artist_booking($field1, $where1, $field2, $where2)
	{
		$data = array();
		$Q =  $this->db->query("SELECT COUNT(ab_no) as total FROM artiste_bookings WHERE ".$field1." = '".$where1."' AND ".$field2." = '".$where2."' ");
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
		$this->db->close();
	}

	public function cek_username($field1, $where1)
	{
		$data = array();
		$Q =  $this->db->query("SELECT COUNT(usr_user_id_pk) as total FROM users WHERE ".$field1." = '".$where1."'");
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
		$this->db->close();
	}

	public function get_booking($field_1, $field_2, $where_1, $where_2)
	{
		return $this->db->query("SELECT * FROM `bookings`
			INNER JOIN users 
			ON users.usr_user_id_pk = bookings.bk_user_id_fk
			INNER JOIN contacts 
			ON contacts.co_contact_id_pk = bookings.bk_contact_id_fk
			WHERE ".$field_1." = '".$where_1."'
			OR ".$field_2." = '".$where_2."'");
		$this->db->close();
	}

	public function get_booking_all()
	{
		$hasil = $this->db->query("SELECT * FROM `bookings`
			INNER JOIN users 
			ON users.usr_user_id_pk = bookings.bk_user_id_fk
			INNER JOIN contacts 
			ON contacts.co_contact_id_pk = bookings.bk_contact_id_fk 
            WHERE bk_date_of_booking >= date('2018-11-17')  
			ORDER BY `bookings`.`bk_date_of_booking` DESC");
		return $hasil->result();
		$this->db->close();
	}

	public function get_booking_all_perpage()
	{
		return $this->db->query("SELECT * FROM `bookings`
			INNER JOIN users 
			ON users.usr_user_id_pk = bookings.bk_user_id_fk
			INNER JOIN contacts 
			ON contacts.co_contact_id_pk = bookings.bk_contact_id_fk 
            WHERE bk_date_of_booking >= date('2018-11-17')  
			ORDER BY `bookings`.`bk_date_of_booking` DESC");
		$this->db->close();
	}

	public function get_booking_all_perpage_like($search)
	{
		return $this->db->query("SELECT * FROM `bookings`
			INNER JOIN users 
			ON users.usr_user_id_pk = bookings.bk_user_id_fk
			INNER JOIN contacts 
			ON contacts.co_contact_id_pk = bookings.bk_contact_id_fk 
            WHERE bk_date_of_booking >= date('2018-11-17') AND bk_event_name LIKE '%".$search."%'  
			ORDER BY `bookings`.`bk_date_of_booking` DESC");
		$this->db->close();
	}

	public function get_count_sales_contact()
	{
		return $this->db->query("SELECT usr_first_name, COUNT(co_user_id_fk) as total FROM `contacts` INNER JOIN users ON users.usr_user_id_pk = contacts.co_user_id_fk GROUP BY usr_username");
		$this->db->close();
	}

	public function get_count_sales_booking()
	{
		return $this->db->query("SELECT usr_first_name, usr_last_name, COUNT(bk_booking_id_pk) as total FROM `bookings` INNER JOIN users ON users.usr_user_id_pk = bookings.bk_contact_id_fk GROUP BY usr_username");
		$this->db->close();
	}

	public function get_booking_1($field,$where)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM `bookings`
			INNER JOIN users 
			ON users.usr_user_id_pk = bookings.bk_user_id_fk
			INNER JOIN contacts 
			ON contacts.co_contact_id_pk = bookings.bk_contact_id_fk
			WHERE ".$field." = '".$where."'");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}






	// MODEL ADD ARTIST IN BOOKING

	public function get_entries()
    {
    	$last_id				= $this->get_last_id_increament('pastiche_crm','bookings');

        $query = $this->web_app_model->getWhereAllJoin('ab_artiste_id_fk','ar_artiste_id_pk','artiste_bookings','artistes','ab_booking_id_fk',$last_id);
        // if (count($query->result()) > 0) {
        return $query->result();
        // }
    }

    public function get_entries_edit($id_booking)
    {

        $query = $this->web_app_model->getWhereAllJoin('ab_artiste_id_fk','ar_artiste_id_pk','artiste_bookings','artistes','ab_booking_id_fk',$id_booking);
        // if (count($query->result()) > 0) {
        return $query->result();
        // }
    }

    public function insert_entry($data)
    {
        return $this->db->insert('artiste_bookings', $data);
    }

    public function check_entry($booking, $artist)
    {
      //  return $this->db->get('artiste_bookings', $data);
        return $this->db->query("SELECT * FROM artiste_bookings WHERE ab_booking_id_fk = '".$booking."' AND ab_artiste_id_fk ='".$artist."'");
    }

    public function delete_entry($id)
    {
        return $this->db->delete('artiste_bookings', array('ab_no' => $id));
    }

    public function single_entry($ab_no)
    {
        $this->db->select('*');
        $this->db->from('artiste_bookings');
        $this->db->where('ab_no', $ab_no);
        $query = $this->db->get();
        if (count($query->result()) > 0) {
            return $query->row();
        }
    }

    public function update_entry($data)
    {
        return $this->db->update('artiste_bookings', $data, array('ab_no' => $data['ab_no']));
    }

}