<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
	
	public function get_uuid()
	{
		/* FOR SQL SERVER ========
		$result = $this->db->query("DECLARE @myid uniqueidentifier = NEWID();  
SELECT CONVERT(char(40), @myid) AS 'Id';");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
		*/

		// FOR MYSQL
		$result = $this->db->query("Select UPPER(REPLACE(UUID(), '-', '')) as Id");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
	}

	public function get_last_id_increament($db,$tbl)
	{

		// FOR MYSQL
		$result = $this->db->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$db."' AND TABLE_NAME = '".$tbl."'
");
		return ($result->num_rows() == 1) ? $result->result()[0]->AUTO_INCREMENT : FALSE;
	}

	//  ID GRUP TELEGRAM AKUN ROBOT
	public function get_id_group_telegram()
	{
		 return "-1001494022054"; // GROUP ID BOT TELEGRAM
	}

	// TOKEN TELEGRAM BOT
	public function get_token_bot_telegram()
	{
		 return "943793671:AAHEqoGkOdzePKamv4otxTCseejbVzVjny4";
		 //PDAM
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */