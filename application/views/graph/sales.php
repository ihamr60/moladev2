<script type="text/javascript">
	Highcharts.chart('sales', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'TOTAL CONTACS / CLIENTS BY SALES'
	  },
	  subtitle: {
	    text: 'Source: Daily Data'
	  },
	  xAxis: {
	    type: 'category',
	    labels: {
	      rotation: -45,
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  },
	  yAxis: {
	    min: 0,
	    title: {
	      text: 'Total Contacts / Clients / Persons'
	    }
	  },
	  legend: {
	    enabled: false
	  },
	  tooltip: {
	    pointFormat: 'Total Contacts / CLients: <b>{point.y} Person</b>'
	  },
	  series: [{
	    name: 'Population',
	    color: '#17a2b8',
	    data: [
	    <?php 
	    	foreach($count_sales_contact->result_array() as $d)
	    	{
	    ?>
	      ['<?php echo $d['usr_first_name'] ?>', <?php echo $d['total'] ?>],
	    <?php } ?>
	    ],

	    dataLabels: {
	      enabled: true,
	      rotation: -90,
	      color: '#eff3f6',
	      align: 'right',
	      format: '{point.y}', // one decimal
	      y: 10, // 10 pixels down from the top
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  }]
	});
</script>

<script type="text/javascript">
	Highcharts.chart('sales2', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'TOTAL BOOKINGS BY SALES'
	  },
	  subtitle: {
	    text: 'Source: Daily Data'
	  },
	  xAxis: {
	    type: 'category',
	    labels: {
	      rotation: -45,
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  },
	  yAxis: {
	    min: 0,
	    title: {
	      text: 'Total Bookings by Sales'
	    }
	  },
	  legend: {
	    enabled: false
	  },
	  tooltip: {
	    pointFormat: 'Total Bookings: <b>{point.y} Bookings</b>'
	  },
	  series: [{
	    name: 'Population',
	    color: '#17a2b8',
	    data: [
	    <?php 
	    	foreach($count_sales_booking->result_array() as $d)
	    	{
	    ?>
	      ['<?php echo $d['usr_first_name'] ?>', <?php echo $d['total'] ?>],
	    <?php } ?>
	    ],
	    dataLabels: {
	      enabled: true,
	      rotation: -90,
	      color: '#eff3f6',
	      align: 'right',
	      format: '{point.y}', // one decimal
	      y: 10, // 10 pixels down from the top
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  }]
	});
</script>