<script>
      document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      height: 'parent',
      themeButtonIcons: {
                prev: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>'
            },
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
      defaultView: 'dayGridMonth',
      defaultDate: '<?php echo date('Y-m-d') ?>',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: false, // allow "more" link when too many events
      events: [
      <?php 
        foreach($data_marketing->result_array() as $d)
        {
      ?>
        {
          title: '[MARKETING] - <?php echo $d['m_note'] ?>',
          start: '<?php echo $d['m_date'] ?>T07:00:00',
          color: '#17a2b8',
        },
      <?php } ?> 
      <?php 
        foreach($get_booking_calendar->result_array() as $d)
        {
      ?>
        {
          title: "<?php echo $d['bk_event_name'] ?>",
          start: '<?php echo date('Y-m-d', strtotime($d['bk_date_of_event'])) ?>',
          color: '#17a2b8',
          url: '<?php echo base_url()?>index.php/<?php echo $kontroller ?>/bg_editBooking/<?php echo $d['bk_booking_id_pk'] ?>?booking=1',
        },
      <?php } ?>
      ]
    });

    calendar.render();
  });
    </script>