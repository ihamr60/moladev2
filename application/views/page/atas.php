<div class="container" style="background-color: #28317a;">
    <div class="navbar-header" style="background-color: #28317a;">
        <!-- start: LOGO -->
         <h3 align="center" class="hidden-xs hidden-md">
            <font color="white" face="Poppins">
                
                <p style="margin-top: 18px; margin-bottom: 18px"><img class="hidden-sm" src="<?php echo base_url(); ?>vendor/assets/images/web/footer-logo-shadow.png" width="28px" style="filter: invert(500%) sepia(100%) saturate(0%) hue-rotate(130deg) brightness(105%) contrast(100%);"> <?php echo $data_config['app_name'] ?></p>
            </font>
        </h3>
         <h3 align="center" class="hidden-lg"><font color="#5f5959"><img style="width: 34px; margin-top: -9px; margin-bottom: 3px; filter: invert(500%) sepia(100%) saturate(0%) hue-rotate(130deg) brightness(105%) contrast(100%);" src="<?php echo base_url(); ?>vendor/assets/images/web/footer-logo-shadow.png"></font></h3>
        <!-- end: LOGO -->
    </div>
    <div class="navbar-tools">
        <!-- start: TOP NAVIGATION MENU -->
        <ul class="nav navbar-right hidden-xs hidden-md">
            <!-- start: USER DROPDOWN -->
            <!--<li class="dropdown current-user">
                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                    <img style="width: 40px; height: 40px;" src="<?php echo base_url(); ?>vendor/assets/images/web/user.png" class="circle-img" alt="">
                    <span class="username"><?php echo $usr_first_name.' '.$usr_last_name ?></span>
                    <i class="clip-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#">
                            <i class="clip-user-2"></i> &nbsp;My Profile
                        </a>
                    </li>
                    <li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/welcome/logout/">
                                <i class="clip-exit"></i> &nbsp;Log Out
                            </a>
                        </li>
               		</li>
                </ul>
            </li> -->
                <!-- end: USER DROPDOWN -->
                <!-- start: PAGE SIDEBAR TOGGLE -->
                
                <!-- end: PAGE SIDEBAR TOGGLE -->
        </ul>
        <!-- end: TOP NAVIGATION MENU -->
    </div>
</div>