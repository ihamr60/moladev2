<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['app_name'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/footer-logo-shadow.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('page/style'); ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container" style="background-color: #eff3f6;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/admin/bg_booking?booking=1">
                                    Bookings
                                </a>
                            </li>
                            <li class="active">
                                Add Artist
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> ADD ARTIST - BOOKING
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="sample-table-#">
                                        <thead>
                                            <tr>
                                                <th width="" class="col-to-export">COMPANY NAME</th>
                                                <th width="250" class="col-to-export">EVENT NAME</th>
                                                <th width="250" class="col-to-export">DATE OF EVENT</th>
                                                <th width="" class="col-to-export">DATE BOOKING TAKEN</th>
                                                <th width="130" class="col-to-export">FINANCIAL PART</th>
                                                <th width="1" class="col-to-export center">STATUS</th>
                                                <th width="1" class="center">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td align=""><b style="text-transform: uppercase;"><?php echo $data_booking['co_company_name'].'</b>
                                                '.($data_booking['co_contact_name'] == '' ? 'Not available' : '<br><i class="clip-user"></i> '.$data_booking['co_contact_name']).' (Contact Name)
                                                '.($data_booking['bk_po_heading'] == '' ? '<br><br>PO Number : Not available' : '<br><br>PO Number : <b>'.$data_booking['bk_po_heading']).'</b>' ?></td>
                                                <td><span style="text-transform: uppercase;"><b>
                                                    <?php echo $data_booking['bk_event_name'].'</b></span>
                                                    '.($data_booking['bk_venue'] == '' ? '<br><i class="clip-location"></i> Not available ' : '<br><i class="clip-location"></i> '.$data_booking['bk_venue']).' (Venue) <br>
                                                    
                                                    '.($data_booking['bk_lead_notes'] == '' ? '<br>Event Address : <br>Not available' : '<br>Event Address : <br><b>'.$data_booking['bk_lead_notes']); ?>
                                                </td>
                                                <td class=""><b style="text-transform: uppercase;">
                                                    <?php echo date('d / F / Y', strtotime($data_booking['bk_date_of_event'])).'</b></span>'.($data_booking['bk_time_of_event'] == '' ? '<br><i class="clip-clock"></i> Not available (Time)' : '<br><i class="clip-clock"></i> '.$data_booking['bk_time_of_event']).'
                                                    '.($data_booking['bk_notes'] == '' ? '<br><br>Notes : <br>Not available' : '<br><br>Notes : <br><b>'.$data_booking['bk_notes']); ?>
                                                </td>
                                                <td class=""><b style="text-transform: uppercase;">
                                                    <?php echo date('d / F / Y', strtotime($data_booking['bk_date_of_booking'])).'</b>'.($data_booking['bk_payment_terms'] == '' ? '<br>Tags : Not available' : '<br>Tags : <b>'.$data_booking['bk_payment_terms']).'</b><br>' ?><br><br>

                                                    <i class="clip-user"></i> <?php echo $data_booking['usr_title'].'. '.$data_booking['usr_first_name'].' '.$data_booking['usr_last_name'] ?> (Sales)
                                                </td>
                                                <td align=""><span ><font color="black">
                                                    <b>INFORMATION:<br><br></b>
                                                    <?php echo ($data_booking['bk_forecast'] == '' ? 'Forecast : -' : 'Forecast : '.$data_booking['bk_forecast']) ?>
                                                    <?php echo ($data_booking['bk_price'] == '' ? '<br>Price : -' : '<br>Price : '.$data_booking['bk_price']) ?> 
                                                    <?php echo ($data_booking['bk_cost'] == '' ? '<br>Cost : -' : '<br>Cost : '.$data_booking['bk_cost']) ?> 
                                                    <?php echo ($data_booking['bk_profit'] == '' ? '<br>Profit : -' : '<br>Profit : '.$data_booking['bk_profit']) ?>
                                                    </span>
                                                </td>
                                                <td class="center"><span style="background-color: #dfe0ea;" class="label"><font color="black">
                                                    <?php echo $data_booking['bk_status'] ?></b></span>
                                                </td>
                                                <td align="center">
                                                    <a style="background-color: #44a2d2;" href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_editBooking/<?php echo $data_booking['bk_booking_id_pk'] ?>/<?php echo $this->uri->segment(4) ?>/<?php echo $this->uri->segment(5) ?>/<?php echo $this->uri->segment(6)?>?booking=1" class="btn btn-xs tooltips" data-placement="top" data-original-title="Edit"><font color="white"><i class="fa fa-pencil-square-o"></i></font></a>
                                                    <a style="background-color: #ec536c;" href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/hapusBooking/<?php echo $data_booking['bk_booking_id_pk'] ?>/<?php echo $this->uri->segment(4) ?>" class="btn btn-xs tooltips" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete it?')"><font color="white"><i class="fa fa-trash-o"></i></font></a></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table><hr>
                                    <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/add_artist_booking/<?php echo $this->uri->segment(3)?>/<?php echo $this->uri->segment(4)?>/<?php echo $this->uri->segment(5)?>/<?php echo $this->uri->segment(6)?>/" method="post">
                                        <div class="modal-body">
                                            <div class="row" style="background-color:white;">
                                                <div class="col-md-5">
                                                    <label><b>SELECT ARTIST :</b></label>
                                                    <p>
                                                        <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="ab_booking_id_fk">
                                                        <select
                                                            name="ab_artiste_id_fk"
                                                            class="form-control"
                                                            required
                                                            >
                                                            <option value="">Please Select Artist</option>
                                                            <?php 
                                                                foreach($data_artist->result_array() as $d)
                                                                {
                                                                    echo '<option value="'.$d['ar_artiste_id_pk'].'">'.$d['ar_act_name'].'</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><b>FEE :</b></label>
                                                    <p>
                                                        <input type="text" class="form-control" name="ar_fee">
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                        <p>
                                                            <b>PAID STATUS :</b>
                                                        </p>
                                                        <select
                                                            id="ab_paid"
                                                            class="form-control"
                                                            required
                                                            >
                                                            <option value="">Select</option>
                                                            <option selected value="0">U N P A I D</option>
                                                            <option value="1">P A I D</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label><b>ACTION:</b></label>
                                                    <p>
                                                        <button type="submit" class="btn" style="background-color: #f5b225; width: 100%;">
                                                            <b><font color="black">  <i class="fa fa-save"></i> SAVE </font></b>
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="100" class="col-to-export">NAME</th>
                                                <th width="140" class="col-to-export center">FEE</th>
                                                <th width="140" class="col-to-export center">PAID STATUS</th>
                                                <th width="60" class="center">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach($data_artist_booking->result_array() as $d)
                                                {
                                            ?>
                                            <tr>
                                                <td style="text-transform: uppercase;" align=""><b><?php echo $d['ar_act_name'] ?></b></td>
                                                <td class="center" style="text-transform: uppercase;">
                                                  <?php echo $d['ab_booking_fee'] ?>
                                                </td>
                                                <td class="center" style="text-transform: uppercase;">
                                                    <?php 
                                                        if($d['ab_paid'] == 1)
                                                        {
                                                            echo "<span style='background-color: #29b348;' class='label'>P A I D</span>";
                                                        }
                                                        else if($d['ab_paid'] == 0)
                                                        {
                                                            echo "<span style='background-color: #ec536c;' class='label'>UNPAID</span>";
                                                        }
                                                    ?>
                                                </td>
                                                <td align="center">
                                                    <a style="background-color: #44a2d2;" data-toggle="modal" data-target="#modalEditArtistBooking<?php echo $d['ab_no'] ?>" class="btn btn-xs tooltips" data-placement="top" data-original-title="Edit"><font color="white"><i class="fa fa-pencil-square-o"></i></font></a>
                                                    <a style="background-color: #ec536c;" href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/hapusArtistBooking/<?php echo $d['ab_no'] ?>/<?php echo $this->uri->segment(3) ?>" class="btn btn-xs tooltips" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete it?')"><font color="white"><i class="fa fa-trash-o"></i></font></a></td>
                                                
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalEditArtistBooking; ?>
            <?php //echo $modalEditUsers; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    <?php echo $menu_atasbawah ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>

</body>

</html>