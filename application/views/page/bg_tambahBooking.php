<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['app_name'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/footer-logo-shadow.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('page/style'); ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
               
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking?booking=1">
                                    Bookings
                                </a>
                            </li>
                            <li class="active">
                                Add
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">ADD BOOKING</b>
                            </div>
                                <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/tambahBooking/<?php echo $this->uri->segment(3) ?>" method="post">
                                    <hr>
                                    <div class="modal-body">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <label>EVENT NAME: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_event_name"
                                                        class="form-control"
                                                        required>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>VENUE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_venue"
                                                        class="form-control"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>EVENT DATE: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <input
                                                        type="date"
                                                        name="bk_date_of_event"
                                                        class="form-control"
                                                        required>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>EVENT TIME: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_time_of_event"
                                                        class="form-control"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12 space12">
                                                <label>DATE BOOKING TAKEN: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <input
                                                        type="date"
                                                        name="bk_date_of_booking"
                                                        class="form-control"
                                                        value="<?php echo date('Y-m-d') ?>"
                                                        required
                                                        >
                                                </p>   
                                            </div>
                                            <div class="col-md-12 space12">
                                                <label>ASSIGNED TO: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_user_id_fk"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <?php
                                                            foreach($data_users->result_array() as $d)
                                                            {
                                                                echo "<option value='".$d['usr_user_id_pk']."'>".$d['usr_first_name']." ".$d['usr_last_name']." (Sales)</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-12 space12">
                                                <label>STATUS: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_status"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="in progress">In Progress</option>
                                                        <option value="lost">Lost</option>
                                                        <option value="won">Won</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>FORECAST: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_forecast"
                                                        class="form-control"
                                                        >
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>PRICE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_price"
                                                        class="form-control"
                                                        >
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>COST: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_cost"
                                                        class="form-control"
                                                        >
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>PROFIT: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_profit"
                                                        class="form-control"
                                                        >
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>EVENT ADDRESS: </label>
                                                <p>
                                                    <textarea
                                                        style="width:100%;height:225px;"
                                                        name="bk_lead_notes"
                                                        class="form-control"
                                                        ></textarea>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="col-md-12 space12">
                                                <label>CLIENT: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_contact_id_fk"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select Client</option>
                                                        <?php 
                                                            foreach($data_client->result_array() as $d)
                                                            {
                                                                echo '<option value="'.$d['co_contact_id_pk'].'">'.$d['co_company_name'].' - (CP: '.$d['co_contact_name'].')</option>';
                                                            }
                                                         ?>
                                                    </select>
                                                </p>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="space12">ARTIST:</label> 
                                                <div class="panel panel-default">
                                                    <div class="modal-body">
                                                        <div class="row" style="background-color:white;">
                                                            <div class="col-md-9">
                                                                <label><b>SELECT ARTIST :</b></label>
                                                                <p>
                                                                    <input type="hidden" value="<?php echo $last_id ?>" id="ab_booking_id_fk">
                                                                    <select
                                                                        id="ab_artiste_id_fk"
                                                                        class="form-control"
                                                                        >
                                                                        <option value="">Please Select Artist</option>
                                                                        <?php 
                                                                            foreach($data_artist->result_array() as $d)
                                                                            {
                                                                                echo '<option value="'.$d['ar_artiste_id_pk'].'">'.$d['ar_act_name'].'</option>';
                                                                            }
                                                                        ?>
                                                                    </select>
                                                                </p>
                                                            </div>
                                                            <!--<div class="col-md-4">
                                                                <label><b>FEE :</b></label>
                                                                <p>
                                                                   <input type="text" value="£0.00" class="form-control" id="ab_booking_fee">
                                                                </p>
                                                            </div>-->
                                                            <!--<div class="col-md-3">
                                                                    <p>
                                                                        <b>PAID STATUS :</b>
                                                                    </p>
                                                                    <select
                                                                        id="ab_paid"
                                                                        class="form-control"
                                                                        required
                                                                        >
                                                                        <option value="">Select</option>
                                                                        <option selected value="0">U N P A I D</option>
                                                                        <option value="1">P A I D</option>
                                                                    </select>
                                                            </div>-->
                                                            <div class="col-md-3">
                                                                <label><b></b></label>
                                                                <p>
                                                                    <button type="button" style="width:100%" class="btn btn-primary" id="add">Add</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>NO</th>
                                                                <th>ARTIST NAME</th>
                                                                <th>FEE</th>
                                                                <th>STATUS PAID</th>
                                                                <th>ACTION</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbody">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>PO NUMBER: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_po_heading"
                                                        class="form-control"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12 space20">
                                                <label>TAGS: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="bk_payment_terms"
                                                        class="form-control"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12 space20">
                                                <label>NOTES (FOR PASTICHE INFORMATION ONLY): </label>
                                                <p>
                                                    <textarea
                                                        style="width:100%;height:225px;"
                                                        name="bk_notes"
                                                        class="form-control"
                                                        ></textarea>
                                                </p>
                                            </div>
                                            
                                        </div>




                                        <!--<div class="row" style="background-color:white;">
                                            <div class="col-md-6">
                                                <h5><b>B. CATEGORY INFORMATION</b></h5> 
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white;">
                                            <div class="col-md-2">
                                                <label>COMMISSION ONLY?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_commission_only"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <label>CONTRACT REQUIRED?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_contract_required"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <label>INVOICE REQUIRED?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_invoice_required"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>WEBSITE ENQUIRY?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_website_enquiry"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>EUROPEAN BOOKING?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_european_booking"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <label>QUALITY LEAD?: <br><font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                        name="bk_quality_lead"
                                                        class="form-control"
                                                        required
                                                        >
                                                        <option value="">Please Select</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </p>
                                            </div>
                                        </div> -->
                                       
                                        <div class="row" style="background-color:white;">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a type="button" href="javascript:history.back()" data-dismiss="modal" class="btn btn-light-grey">
                                            CANCEL
                                        </a>
                                        <button style="background: #28317a; color: white;" type="submit" class="btn">
                                            SAVE BOOKING <i class="clip-chevron-right"></i>
                                        </button>
                                    </div>
                                </form>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <!-- Edit Modal -->
            <div class="modal fade" id="editModal" data-width="560" style="display: none;"
                >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Artiste</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="update_form">
                            <input type="hidden" id="ab_no">
                            <div class="form-group">
                                <label for="">Status Paid</label>
                                <select
                                    id="select2insidemodal"
                                    class="form-control"
                                    required
                                    >
                                    <option value="">S E L E C T</option>
                                    <option value="0">U N P A I D</option>
                                    <option value="1">P A I D</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Fee</label>
                                <input type="text" class="form-control" id="fee">
                            </div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="update">Update</button>
                    </div>
                </div>
            </div>
            <?php //echo $modalTambahBooking; ?>
            <?php //echo $modalEditUsers; ?>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <?php echo $this->session->flashdata('info2'); ?>
    <?php echo $menu_atasbawah ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>

    <script>

        $(document).ready(function() {
          $("#select2insidemodal").select2({
            width: '100%', 
            dropdownParent: $("#editModal")
          });
        });

    </script>

   

    <script>
    $(document).on('click', '#add', function(e) {
        e.preventDefault();

        var ab_artiste_id_fk    = $("#ab_artiste_id_fk").val();
        var ab_booking_id_fk    = $("#ab_booking_id_fk").val();
        var ab_booking_fee      = "";
        var ab_paid             = "0";

        $.ajax({
            url: "<?php echo base_url(); ?>index.php/welcome/insert",
            type: "post",
            dataType: "json",
            data: {

                ab_artiste_id_fk: ab_artiste_id_fk,
                ab_booking_id_fk: ab_booking_id_fk,
                ab_booking_fee: ab_booking_fee,
                ab_paid: ab_paid,
            },
            success: function(data) {

                if (data.response == "success") {
                    fetch();
                    $('#exampleModal').modal('hide')
                  //  $("#form")[0].reset();
                    Command: toastr["success"](data.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                } else {
                    Command: toastr["error"](data.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                }
            }
        });

    });

    function fetch() {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/welcome/fetch",
            type: "get",
            dataType: "json",
            success: function(data) {
                var i = 1;
                var tbody = "";
                for (var key in data) {
                    tbody += "<tr>";
                    tbody += "<td class='center'>" + i++ + "</td>";
                    tbody += "<td>" + data[key]['ar_act_name'] + "</td>";
                    tbody += "<td>" + (data[key]['ab_booking_fee'] == '' ? '0.00' : data[key]['ab_booking_fee']) + "</td>";
                    tbody += "<td>" + (data[key]['ab_paid'] == '1' ? '<span style="background-color: #29b348;" class="label">P A I D</span>' : '<span style="background-color: #ec536c;" class="label">UNPAID</span>') + "</td>";
                    tbody += `<td>
                                    <a class="btn btn-teal btn-xs tooltips" href="#" id="edit" value="${data[key]['ab_no']}"><font color="white"><i class="fa fa-pencil"></i> Edit</font></a>
                                    <a class="btn btn-xs tooltips" style="background-color: #ec536c;" href="#" id="del" value="${data[key]['ab_no']}"><font color="white"><i class="fa fa-trash-o"></i> Delete</font></a>
                                </td>`;
                    tbody += "<tr>";
                }

                $("#tbody").html(tbody);
            }
        });
    }

    fetch();

    $(document).on("click", "#del", function(e) {
        e.preventDefault();

        var ab_no = $(this).attr("value");

        if (ab_no == "") {
            alert("Delete id required");
        } else {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-2'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php/welcome/delete",
                        type: "post",
                        dataType: "json",
                        data: {
                            ab_no: ab_no
                        },
                        success: function(data) {
                            fetch();
                            if (data.response === 'success') {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    'Your data has been deleted.',
                                    'success'
                                )
                            }
                        }
                    });

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            });
        }

    });

    $(document).on("click", "#edit", function(e) {
        e.preventDefault();

        var ab_no = $(this).attr("value");

        if (ab_no == "") {
            alert("Edit id required");
        } else {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php/welcome/edit",
                type: "post",
                dataType: "json",
                data: {
                    ab_no: ab_no
                },
                success: function(data) {
                    if (data.response === 'success') {
                        $('#editModal').modal('show');
                        $("#ab_no").val(data.post.ab_no);
                        $("#select2insidemodal").val(data.post.ab_paid);
                         $("#fee").val(data.post.ab_booking_fee);

                    } else {
                        Command: toastr["error"](data.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                }
            });
        }
    });

    $(document).on("click", "#update", function(e) {
        e.preventDefault();

        var ab_no = $("#ab_no").val();
        var ab_paid = $("#select2insidemodal").val();
        var ab_booking_fee = $("#fee").val();

        if (ab_no == "" || ab_paid == "") {
            alert("Paid status field is required");
        } else {

            $.ajax({
                url: "<?php echo base_url(); ?>index.php/welcome/update",
                type: "post",
                dataType: "json",
                data: {
                    ab_no: ab_no,
                    ab_paid: ab_paid,
                    ab_booking_fee: ab_booking_fee
                },
                success: function(data) {
                    fetch();
                    if (data.response === 'success') {
                        $('#editModal').modal('hide');
                        Command: toastr["success"](data.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        Command: toastr["error"](data.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                }
            });
            $("#update_form")[0].reset();
        }

    });

    </script>

</body>

</html>