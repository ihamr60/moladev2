<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['app_name'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/footer-logo-shadow.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
     <?php $this->load->view('page/style'); ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
            <?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client?client=1">
                                    Clients
                                </a>
                            </li>
                            <li class="active">
                                Edit
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> EDIT CLIENT
                                
                            </div>
                                <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/editContact/" method="post">
                                    
                                    <div class="modal-body">
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <h5><b>A. COMPANY DATA</b></h5> 
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <label>COMPANY NAME: </label>
                                                <p>
                                                    <input type="hidden" value="<?php echo $data_client['co_contact_id_pk'] ?>" name="co_contact_id_pk">
                                                    <input
                                                        type="text"
                                                        name="co_company_name"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_name'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>COMPANY WEBSITE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_website"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_website'] ?>">
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>COMPANY TELEPHONE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_telephone"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_telephone'] ?>"
                                                        >
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <h5><b>B. ADDRESS DATA</b></h5> 
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <label>ADDRESS LINE 1: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_address_line1"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_address_line1'] ?>"
                                                        minlenght="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>ADDRESS LINE 2: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_address_line2"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_address_line2'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>ADDRESS LINE 3: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_address_line3"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_address_line3'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>COMPANY TOWN: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_town"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_town'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>POST CODE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_company_postcode"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_company_postcode'] ?>"
                                                        >
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <h5><b>C. CATEGORY CONTACT</b></h5> 
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <label>MAILSHOT ?: </label>
                                                <p>
                                                    <select
                                                    name="co_mailshot_sent"
                                                    class="form-control"
                                                    >
                                                    <?php
                                                        if($data_client['co_mailshot_sent'] == 1 || $data_client['co_mailshot_sent'] == 'Y')
                                                        {
                                                            echo '<option selected value="1">Yes</option>
                                                                    <option value="0">No</option>';
                                                        }
                                                        else if($data_client['co_mailshot_sent'] == 0 || $data_client['co_mailshot_sent'] == 'N')
                                                        {
                                                            echo '<option value="1">Yes</option>
                                                                    <option selected value="0">No</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>CONTACTED ?: </label>
                                                <p>
                                                    <select
                                                    name="co_contacted"
                                                    class="form-control"
                                                    >
                                                       <?php
                                                            if($data_client['co_contacted'] == 1 || $data_client['co_contacted'] == 'Y')
                                                            {
                                                                echo '<option selected value="1">Yes</option>
                                                                        <option value="0">No</option>';
                                                            }
                                                            else if($data_client['co_contacted'] == 0 || $data_client['co_contacted'] == 'N')
                                                            {
                                                                echo '<option value="1">Yes</option>
                                                                        <option selected value="0">No</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>QUOTED ?: </label>
                                                <p>
                                                    <select
                                                    name="co_quoted"
                                                    class="form-control"
                                                    >
                                                        <?php
                                                            if($data_client['co_quoted'] == 1 || $data_client['co_quoted'] == 'Y')
                                                            {
                                                                echo '<option selected value="1">Yes</option>
                                                                        <option value="0">No</option>';
                                                            }
                                                            else if($data_client['co_quoted'] == 0 || $data_client['co_quoted'] == 'N')
                                                            {
                                                                echo '<option value="1">Yes</option>
                                                                        <option selected value="0">No</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>PROSPECT ?: </label>
                                                <p>
                                                    <select
                                                    name="co_prospect"
                                                    class="form-control"
                                                    >
                                                        <?php
                                                            if($data_client['co_prospect'] == 1 || $data_client['co_prospect'] == 'Y')
                                                            {
                                                                echo '<option selected value="1">Yes</option>
                                                                        <option value="0">No</option>';
                                                            }
                                                            else if($data_client['co_prospect'] == 0 || $data_client['co_prospect'] == 'N')
                                                            {
                                                                echo '<option value="1">Yes</option>
                                                                        <option selected value="0">No</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>CLIENT ?: </label>
                                                <p>
                                                    <select
                                                    name="co_client"
                                                    class="form-control"
                                                    >
                                                        <?php
                                                            if($data_client['co_client'] == 1 || $data_client['co_client'] == 'Y')
                                                            {
                                                                echo '<option selected value="1">Yes</option>
                                                                        <option value="0">No</option>';
                                                            }
                                                            else if($data_client['co_client'] == 0 || $data_client['co_client'] == 'N')
                                                            {
                                                                echo '<option value="1">Yes</option>
                                                                        <option selected value="0">No</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </p>   
                                            </div>
                                            <div class="col-md-12">
                                                <label>NIGHTCLUB ?: </label>
                                                <p>
                                                    <select
                                                    name="co_nightclub"
                                                    class="form-control"
                                                    required>
                                                        <?php
                                                            if($data_client['co_nightclub'] == 1 || $data_client['co_nightclub'] == 'Y')
                                                            {
                                                                echo '<option selected value="1">Yes</option>
                                                                        <option value="0">No</option>';
                                                            }
                                                            else if($data_client['co_nightclub'] == 0 || $data_client['co_nightclub'] == 'N')
                                                            {
                                                                echo '<option value="1">Yes</option>
                                                                        <option selected value="0">No</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </p>   
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <h5><b>D. CONTACT DATA</b></h5> 
                                            </div>
                                        </div>
                                        <div class="row" style="background-color:white">
                                            <div class="col-md-12">
                                                <label>CONTACT NAME: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_contact_name"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_contact_name'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>POSITION: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_contact_position"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_contact_position'] ?>"
                                                        minlength="5"
                                                        >
                                                </p>               
                                            </div>
                                            <div class="col-md-12">
                                                <label>CONTACT TELEPHONE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_contact_telephone"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_contact_telephone'] ?>"
                                                        >
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>CONTACT MOBILE: </label>
                                                <p>
                                                    <input
                                                        type="text"
                                                        name="co_contact_mobile"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_contact_mobile'] ?>"
                                                        >
                                                </p>            
                                            </div>
                                            <div class="col-md-12">
                                                <label>CONTACT E-MAIL: </label>
                                                <p>
                                                    <input
                                                        type="email"
                                                        name="co_contact_email"
                                                        class="form-control"
                                                        value="<?php echo $data_client['co_contact_email'] ?>">
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>ASSIGNED TO: <font size="0" color="red">(*required)</font></label>
                                                <p>
                                                    <select
                                                    name="co_user_id_fk"
                                                    class="form-control"
                                                    required>
                                                        <?php 
                                                            foreach($data_users->result_array() as $e)
                                                            {
                                                                if($data_client['co_user_id_fk'] == $e['usr_user_id_pk'])
                                                                {
                                                                    echo '<option selected value="'.$e['usr_user_id_pk'].'">'.$e['usr_first_name'].' '.$e['usr_last_name'].'</option>';
                                                                }
                                                                else if($data_client['co_user_id_fk'] != $e['usr_user_id_pk'])
                                                                {
                                                                    echo '<option value="'.$e['usr_user_id_pk'].'">'.$e['usr_first_name'].' '.$e['usr_last_name'].'</option>';
                                                                }    
                                                            } 
                                                        ?>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>NOTE: </label>
                                                <p>
                                                    <textarea
                                                        style="width:100%;height:150px;"
                                                        name="co_notes"
                                                        class="form-control"
                                                        ><?php echo $data_client['co_notes'] ?></textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="javascript:history.back()" type="button" data-dismiss="modal" class="btn btn-light-grey">
                                            CANCEL
                                        </a>
                                        <button style="background: #28317a; color: white;" type="submit" class="btn">
                                            UPDATE
                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php //echo $modalTambahBooking; ?>
            <?php //echo $modalEditUsers; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    <?php echo $menu_atasbawah ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>

</body>

</html>