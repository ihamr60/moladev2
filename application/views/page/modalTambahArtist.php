<div id="modalTambahArtist" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/tambahArtist/" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            
            <h4 class="modal-title"><img src="<?php echo base_url() ?>vendor/assets/images/icon/sign-form.png" width="25px"><b> - ADD ARTIST</b></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <h5><b>A. GENERAL DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>ACT NAME: <font size="0" color="red">(*required)</font></label>
                    <p>
                        <input
                            type="text"
                            name="ar_act_name"
                            class="form-control"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>WEBSITE: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_website"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ENTERTAINMENT: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_entertainment_type"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>FEE: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_fee"
                            class="form-control"
                            >
                    </p>
                </div>
                <!--<div class="col-md-4">
                    <label>BAND ?: </label>
                    <p>
                        <select
                        name="ar_band"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-4">
                    <label>GENRE: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_band_genre"
                            class="form-control"
                            placeholder="Ex: Jazz"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>LINK PORTFOLIO: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_portfolio_url"
                            class="form-control"
                            placeholder="Type Here ..."
                            >
                    </p>
                </div>-->
            </div>
            <hr>
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <h5><b>B. ADDRESS DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>ADDRESS LINE 1: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_address_line1"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ADDRESS LINE 2: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_address_line2"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ADDRESS LINE 3: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_address_line3"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>COUNTRY: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_county"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>POST CODE: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_postcode"
                            class="form-control"
                            >
                    </p>
                </div>
            </div>
            <hr>
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <h5><b>C. CONTACT DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>CONTACT NAME: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_contact_name"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>TELEPHONE: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_telephone"
                            class="form-control"   
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>E-MAIL: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_email"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>LOCATION: </label>
                    <p>
                        <input
                            type="text"
                            name="ar_location"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NOTE: </label>
                    <p>
                        <textarea
                            style="width:100%;height:100%;"
                            name="ar_notes"
                            class="form-control"
                            ></textarea>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button style="background: #28317a; color: white;" type="submit" class="btn">
                SAVE
            </button>
        </div>
    </form>
</div>