<?php
    foreach($data_artist_booking->result_array() as $d)
    {
?>

<div id="modalEditArtistBooking<?php echo $d['ab_no'] ?>" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/editArtistBooking" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            
            <h4 class="modal-title"><img src="<?php echo base_url() ?>vendor/assets/images/icon/sign-form.png" width="25px"><b> - UPDATE ARTIST BOOKING</b></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>FEE:<font color="red">*</font></label>
                    <p>
                        <input type="hidden" value="<?php echo $d['ab_no'] ?>" name="ab_no">
                        <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="bk_booking_id_pk">
                        <input
                            type="text"
                            name="ab_booking_fee"
                            class="form-control"
                            value="<?php echo $d['ab_booking_fee'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="panel-body">
                        <p>
                            PAID STATUS
                        </p>
                        <label class="radio-inline">
                            <input type="radio" value="1" name="ab_paid" class="grey">
                            P A I D
                        </label>
                        <label class="radio-inline">
                            <input type="radio" value="0" name="ab_paid" class="grey">
                            U N P A I D
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button style="background: #28317a; color: white;" type="submit" class="btn">
                UPDATE
            </button>
        </div>
    </form>
</div>

<?php } ?>