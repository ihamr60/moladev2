<?php
    foreach($data_marketing->result_array() as $d)
    {
?>
<div id="modalEditMarketing<?php echo $d['m_id'] ?>" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/editMarketing" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            
            <h4 class="modal-title"><img src="<?php echo base_url() ?>vendor/assets/images/icon/sign-form.png" width="25px"><b> - EDIT MARKETING SCHEDULE PASTICHE CRM</b></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>DATE: <font size="0" color="red">*(required)</font></label>
                    <p>
                        <input type="hidden" value="<?php echo $d['m_id'] ?>" name="m_id">
                        <input
                            type="date"
                            name="m_date"
                            class="form-control"
                            value="<?php echo $d['m_date'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>NOTE: <font size="0" color="red">*(required)</font></label>
                    <p>
                        <textarea
                            onkeypress="return noenter(event)"
                            style="width:100%;height:100%;"
                            name="m_note"
                            class="form-control"
                            placeholder="Type here ..."
                            ><?php echo $d['m_note'] ?></textarea>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button style="background: #28317a; color: white;" type="submit" class="btn">
                UPDATE
            </button>
        </div>
    </form>
</div>
<?php } ?>