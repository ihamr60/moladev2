<div id="modalTambahClient" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/tambahContact/<?php echo  $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5) ?>" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            
            <h4 class="modal-title"><img src="<?php echo base_url() ?>vendor/assets/images/icon/sign-form.png" width="25px"><b> - ADD CLIENT</b></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <h5><b>A. COMPANY DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <label>COMPANY NAME: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_name"
                            class="form-control"
                            minlength="5">
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>COMPANY WEBSITE: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_website"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>COMPANY TELEPHONE: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_telephone"
                            class="form-control">
                    </p>
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <h5><b>B. ADDRESS DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <label>ADDRESS LINE 1: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_address_line1"
                            class="form-control"
                            minlenght="5"
                            >
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ADDRESS LINE 2: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_address_line2"
                            class="form-control"
                            minlength="5"
                            >
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ADDRESS LINE 3: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_address_line3"
                            class="form-control"
                            minlength="5"
                            >
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>COMPANY TOWN: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_town"
                            class="form-control"
                            minlenght="5"
                            >
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>POST CODE: </label>
                    <p>
                        <input
                            type="text"
                            name="co_company_postcode"
                            class="form-control"
                            >
                    </p>
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <h5><b>C. CATEGORY CONTACT</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <label>MAILSHOT ?: </label>
                    <p>
                        <select
                        name="co_mailshot_sent"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-12">
                    <label>CONTACTED ?: </label>
                    <p>
                        <select
                        name="co_contacted"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-12">
                    <label>QUOTED ?: </label>
                    <p>
                        <select
                        name="co_quoted"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-12">
                    <label>PROSPECT ?: </label>
                    <p>
                        <select
                        name="co_prospect"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-12">
                    <label>CLIENT ?: </label>
                    <p>
                        <select
                        name="co_client"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
                <div class="col-md-12">
                    <label>NIGHTCLUB ?: </label>
                    <p>
                        <select
                        name="co_nightclub"
                        class="form-control"
                        >
                            <option value="">Please Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </p>   
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <h5><b>D. CONTACT DATA</b></h5> 
                </div>
            </div>
            <div class="row" style="background-color:white">
                <div class="col-md-12">
                    <label>CONTACT NAME: </label>
                    <p>
                        <input
                            type="text"
                            name="co_contact_name"
                            class="form-control"
                            minlength="5">
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>POSITION: </label>
                    <p>
                        <input
                            type="text"
                            name="co_contact_position"
                            class="form-control"
                            minlength="5">
                            <font color="blue" size="0">Note: Minlenght 5 Character</font>
                    </p>               
                </div>
                <div class="col-md-12">
                    <label>CONTACT TELEPHONE: </label>
                    <p>
                        <input
                            type="text"
                            name="co_contact_telephone"
                            class="form-control"
                            >
                            <font color="blue" size="0">Telephone</font>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>CONTACT MOBILE: </label>
                    <p>
                        <input
                            type="text"
                            name="co_contact_mobile"
                            class="form-control">
                    </p>            
                </div>
                <div class="col-md-12">
                    <label>CONTACT E-MAIL: </label>
                    <p>
                        <input
                            type="email"
                            name="co_contact_email"
                            class="form-control"
                            >
                    </p>
                </div>
                <div class="col-md-12">
                    <label>ASSIGNED TO: <font size="0" color="red">(required)</font></label>
                    <p>
                        <select
                        name="co_user_id_fk"
                        class="form-control"
                        required>
                            <option value="">Please Select</option>
                            <?php 
                                foreach($data_users->result_array() as $d)
                                {
                                    echo '<option value="'.$d['usr_user_id_pk'].'">'.$d['usr_first_name'].' '.$d['usr_last_name'].'</option>';
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NOTE: </label>
                    <p>
                        <textarea
                            style="width:100%;height:150px;"
                            name="co_notes"
                            class="form-control"
                            ></textarea>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button style="background: #28317a; color: white;" type="submit" class="btn">
                SAVE
            </button>
        </div>
    </form>
</div>