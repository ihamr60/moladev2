<style>
body {margin:0;}

.menu1 {
  overflow: hidden;
  /*background-color: #f5f5f5;*/
  /*position: fixed;*/
  top: 130px;
  width: 100%;
 /* border-bottom: 1px solid #c8c7cc; */
}

.menu1 a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 8px 0px;
  text-decoration: none;
  font-size: 13px;
 
  
   
    width: 19%;
    margin: 0 0 0 5%;
   
}

.menu1 a:hover {
  background: #f1f1f1;
  color: black;
}

.menu1 a.active {
  background-color: #7dc5f7;
  color: white;
}




.bnavbar {
  overflow: hidden;
  background-color: #f5f5f5;
  position: fixed;
  top: 61px;
  width: 100%;
  border-bottom: 1px solid #c8c7cc;
}

.bnavbar a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 10px 0px;
  text-decoration: none;
  font-size: 13px;
 
  
 	 
    width: 27%;
    margin: 0 0 0 5%;
   
}

.bnavbar a:hover {
  background: #f1f1f1;
  color: black;
}

.bnavbar a.active {
  background-color: #7dc5f7;
  color: white;
}











.anavbar {
  overflow: hidden;
  background-color: #f5f5f5;
  position: fixed;
  bottom: 0;
  width: 100%;
  border-top: 1px solid #c8c7cc;
}

.anavbar a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 8px 0px;
  text-decoration: none;
  font-size: 13px;
 
  
 	 
   /* width: 19%; */
  width: 30%;
  /* margin: 0 0 0 5%; */
  margin: 0 0 0 13%;
   
}

.anavbar a:hover {
  background: #f1f1f1;
  color: black;
}

.anavbar a.active {
  background-color: #7dc5f7;
  color: white;
}

.main {
  padding: 16px;
  margin-bottom: 30px;
}



.counter{
	float: left;
	border: 0px solid #c8c7cc;
	
	
	padding: 8px 0px;
	width: 14%;
    margin: 0 0 0 9%;
	height: 14%;
	background-color: #d9edf7;
	
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
}


.counter_layanan{
	float: left;
	border: 0px solid #c8c7cc;
	
	
	padding: 8px 0px;
	width: 14%;
    margin: 0 0 0 15%;
	height: 14%;
	/*background-color: #d9edf7; */
	
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
}



</style>

<style type="text/css">
  /* style untuk link popup */
  a.popup-link {
    padding:17px 0;
    text-align: center;
    margin:10% auto;
    position: relative;
    width: 300px;
    color: #fff;
    text-decoration: none;
    background-color: #FFBA00;
    border-radius: 3px;
    box-shadow: 0 5px 0px 0px #eea900;
    display: block;
  }
  a.popup-link:hover {
    background-color: #ff9900;
    box-shadow: 0 3px 0px 0px #eea900;
    -webkit-transition:all 1s;
    transition:all 1s;
  }
  /* end link popup*/
  /* animasi popup */

  @-webkit-keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  @-moz-keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  @keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  /* end animasi popup */
  /*style untuk popup */  
  #popup {
    background-color: rgba(0,0,0,0.8);
    position: fixed;
    top:0;
    left:0;
    right:0;
    bottom:0;
    margin:0;
    -webkit-animation:autopopup 2s;
    -moz-animation:autopopup 2s;
    animation:autopopup 2s;
  }
  #popup:target {
    -webkit-transition:all 1s;
    -moz-transition:all 1s;
    transition:all 1s;
    opacity: 0;
    visibility: hidden;
  }

  @media (min-width: 768px){
    .popup-container {
      width:600px;
    }
  }
  @media (max-width: 767px){
    .popup-container {
      width:100%;
    }
  }
  .popup-container {
    position: relative;
    margin:7% auto;
    padding:30px 50px;
    background-color: #fafafa;
    color:#333;
    border-radius: 3px;
  }

  a.popup-close {
    position: absolute;
    top:3px;
    right:3px;
    background-color: #333;
    padding:7px 10px;
    font-size: 20px;
    text-decoration: none;
    line-height: 1;
    color:#fff;
  }
  /* end style popup */

  /* style untuk isi popup */
  .popup-form {
    margin:10px auto;
  }
    .popup-form h2 {
      margin-bottom: 5px;
      font-size: 37px;
      text-transform: uppercase;
    }
    .popup-form .input-group {
      margin:10px auto;
    }
      .popup-form .input-group input {
        padding:17px;
        text-align: center;
        margin-bottom: 10px;
        border-radius:3px;
        font-size: 16px; 
        display: block;
        width: 100%;
      }
      .popup-form .input-group input:focus {
        outline-color:#FB8833; 
      }
      .popup-form .input-group input[type="email"] {
        border:0px;
        position: relative;
      }
      .popup-form .input-group input[type="submit"] {
        background-color: #FB8833;
        color: #fff;
        border: 0;
        cursor: pointer;
      }
      .popup-form .input-group input[type="submit"]:focus {
        box-shadow: inset 0 3px 7px 3px #ea7722;
      }
  /* end style isi popup */

  body {
            font-family: Poppins;
        }

  .breadcrumb {
      background-color: #eff3f6;
      border-bottom: 0px solid #ffffff;
  }

  .breadcrumb a {
      color: #28317a;
  }

  .main-navigation {
      background-color: white;
  }

  .main-container {
      background-color: white;
  }

  .footer {
      background-color: white;
  }

  .row {
      background-color: #eff3f6;
  }

  ul.main-navigation-menu>li a {
    border-bottom: none;
    border-top-color: white;
  }

  ul.main-navigation-menu>li.open, ul.main-navigation-menu>li:hover>a, ul.main-navigation-menu>li>a:hover {
    background-color: #dfe0ea;
  }


  table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting{
    background-image: unset !important;
  }

  table.dataTable thead .sorting_desc{
    background-image: unset !important;
  }

  </style>