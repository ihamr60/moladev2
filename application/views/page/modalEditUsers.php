<?php
    foreach($data_users->result_array() as $d)
    {
?>

<div id="modalEditUsers<?php echo $d['usr_user_id_pk'] ?>" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/admin/editUsers" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            
            <h4 class="modal-title"><img src="<?php echo base_url() ?>vendor/assets/images/icon/sign-form.png" width="25px"><b> - EDIT USERS PASTICHE CRM</b></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="background-color:white;">
                <div class="col-md-12">
                    <label>TITLE:<font color="red">*</font></label>
                    <p>
                        <input type="hidden" value="<?php echo $d['usr_user_id_pk'] ?>" name="usr_user_id_pk">
                        <select
                        name="usr_title"
                        class="form-control"
                        required>
                        <?php
                            if($d['usr_title'] == 'Mr')
                            { 
                                echo '<option selected value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>';
                            } 
                            else if($d['usr_title'] == 'Mrs')
                            { 
                                echo '<option value="Mr">Mr</option>
                                        <option selected value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>';
                            } 
                            else if($d['usr_title'] == 'Miss')
                            { 
                                echo '<option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option selected value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>';
                            } 
                            else if($d['usr_title'] == 'Ms')
                            { 
                                echo '<option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option selected value="Ms">Ms</option>';
                            } 
                        ?>
                        </select>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>USERNAME:<font color="red">*</font></label>
                    <p>
                        <input
                            type="text"
                            name="usr_username"
                            class="form-control"
                            minlength="6"
                            maxlength="10"
                            value="<?php echo $d['usr_username'] ?>"
                            readonly>
                             <font color="red">**</font> <font size="0">Notice: <b>Default password</b> is the same as username</font>
                    </p>
                   
                </div>
                <div class="col-md-6">
                    <label>FIRST NAME:<font color="red">*</font></label>
                    <p>
                        <input
                            type="text"
                            name="usr_first_name"
                            class="form-control"
                            value="<?php echo $d['usr_first_name'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-6">
                    <label>LAST NAME:<font color="red">*</font></label>
                    <p>
                        <input
                            type="text"
                            name="usr_last_name"
                            class="form-control"
                            value="<?php echo $d['usr_last_name'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>E-MAIL ADDRESS:<font color="red">*</font></label>
                    <p>
                        <input
                            type="email"
                            name="usr_email_address"
                            class="form-control"
                            value="<?php echo $d['usr_email_address'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>ROLE:<font color="red">*</font></label>
                    <p>
                        <select
                        name="usr_security_profile_id_fk"
                        class="form-control"
                        required>
                        <?php 
                            if($d['usr_security_profile_id_fk'] == 1)
                            {
                                echo '<option selected value="1">Administrator</option>
                                        <option value="2">Sales</option>';
                            }
                            else if($d['usr_security_profile_id_fk'] == 2)
                            {
                                echo '<option value="1">Administrator</option>
                                        <option selected value="2">Sales</option>';
                            }
                        ?>
                        </select>
                    </p>
                   
                </div>
                <!--<div class="col-md-6">
                    <label>STATUS:<font color="red">*</font></label>
                    <p>
                        <select
                        name="usr_active"
                        class="form-control"
                        required>
                        <?php 
                            if($d['usr_active'] == 1)
                            {
                                echo '<option selected value="1">Active</option>
                                        <option value="0">Non-Active</option>';
                            }
                            else if($d['usr_active'] == 0)
                            {
                                echo '<option value="1">Active</option>
                                        <option selected value="0">Non-Active</option>';
                            }
                        ?>
                            
                        </select>
                    </p>
                   
                </div>-->
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button style="background: #28317a; color: white;" type="submit" class="btn">
                UPDATE
            </button>
        </div>
    </form>
</div>

<?php } ?>