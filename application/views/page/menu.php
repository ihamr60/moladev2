<div class="navigation-toggler">
    <i class="clip-chevron-left"></i>
    <i class="clip-chevron-right"></i>
</div>
<ul class="main-navigation-menu" style="background-color: white;">
    <li>
        <a <?php if (isset($_GET['booking']) && $_GET['booking']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking?booking=1">
            <font color="#28317a"><i class="clip-database"></i></font>
            <span class="title">BOOKINGS </span><!--<i class="icon-arrow"></i>-->
        </a>
        <!--<ul class="sub-menu" style="background-color:white;">
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_commission_only/1/Y?booking=1">
                    <i class="fa fa-square-o"></i>
                    <span class="title"> Commission Only </span>
                </a>
            </li>
             <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_status?stts1=in progress&booking=1">
                    <span class="title"> In Progress </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_status?stts1=won&booking=1">
                    <span class="title">Won </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_status?stts1=lost&booking=1">
                    <span class="title">Lost </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_website_enquiry?stts1=1&stts2=Y&booking=1">
                    <span class="title"> Website Enquiry </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_european_booking?stts1=1&stts2=Y&booking=1">
                    <span class="title">European Booking </span>
                </a>
            </li>
           
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_quality_lead/1/Y?booking=1">
                    <i class="fa fa-square-o"></i>
                    <span class="title"> Quality Lead </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_contract_required/1/Y?booking=1">
                    <i class="fa fa-square-o"></i>
                    <span class="title">Contract Required </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_booking/bk_invoice_required/1/Y?booking=1">
                    <i class="fa fa-square-o"></i>
                    <span class="title">Invoice Required </span>
                </a>
            </li>
        </ul>-->
    </li>
    <li>
        <a <?php if (isset($_GET['client']) && $_GET['client']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client?client=1">
            <font color="#28317a"><i class="clip-phone-2"></i></font>
            <span class="title">CLIENTS</span><!--<i class="icon-arrow"></i>-->
        </a>
        <!--<ul class="sub-menu" style="background-color:white;">
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_contacted/1/Y?client=1">
                    <span class="title"> Contacted </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_quoted/1/Y?client=1">
                    <span class="title"> Quoted </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_nightclub/1/Y?client=1">
                    <span class="title">Nightclub </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_prospect/1/Y?client=1">
                    <span class="title"> Prospect </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_client/1/Y?client=1">
                    <span class="title">Client </span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_client/co_mailshot_sent/1/Y?client=1">
                    <span class="title">Mailshot </span>
                </a>
            </li>
        </ul>-->
    </li>
    <li>
        <a <?php if (isset($_GET['artist']) && $_GET['artist']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/bg_artist?artist=1">
            <font color="#28317a"><i class="clip-users-3"></i></font>
            <span class="title">ARTISTES</span>
        </a>
    </li>
    <!--<li>
        <a <?php if (isset($_GET['marketing']) && $_GET['marketing']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/bg_marketing?marketing=1">
            <font color="#28317a"><i class="clip-calendar"></i></font>
            <span class="title">MARKETING</span>
        </a>
    </li>-->
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #dfe0ea; "';}?> 
            href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>?home=1">
            <font color="#28317a"><i class="clip-calendar"></i></font>
            <span class="title">CALENDAR</span>
        </a>
    </li>
    <?php
        if($kontroller == 'admin')
        {
    ?>
    <li>
        <a <?php if (isset($_GET['users']) && $_GET['users']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/bg_users?users=1">
            <font color="#28317a"><i class="clip-user"></i></font>
            <span class="title">USERS</span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['config']) && $_GET['config']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/bg_config?config=1">
            <font color="#28317a"><i class="clip-cog"></i></font>
            <span class="title">E-MAIL NOTIFICATION</span>
        </a>
    </li>
<?php } ?>
    <li>
        <a <?php if (isset($_GET['password']) && $_GET['password']==1){echo 'style="background: #dfe0ea; "';}?> 
        href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/bg_password?password=1">
            <font color="#28317a"><i class="clip-cog"></i></font>
            <span class="title">CHANGE PASSWORD</span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['logout']) && $_GET['logout']==1){echo 'style="background: #ssss; "';}?> 
        href="<?php echo base_url();?>index.php/welcome/logout/">
            <font color="#28317a"><i class="clip-exit"></i></font>
            <span class="title">LOGOUT</span>
        </a>
    </li>
</ul>