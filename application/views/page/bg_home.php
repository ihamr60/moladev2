<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['app_name'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/footer-logo-shadow.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('page/style'); ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url(); ?>vendor/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />

    <link href='<?php echo base_url(); ?>vendor/calendar/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />


    <link href='<?php echo base_url(); ?>vendor/calendar/fullcalendar/packages/core/main.css' rel='stylesheet' />



    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
        
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container" style="background-color: #eff3f6;">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                Calendar
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">
                    <div class="col-sm-12 hidden-xs hidden-md">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="clip-stats"></i>CALENDAR SCHEDULE PASTICHE MANAGEMENT
                            </div>
                            <div class="panel-body">
                                <div id='calendar-container'>
                                   <div style="width: 100%; height: 100%;" class="panel" id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-sm-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="clip-stats"></i>TOTAL CONTACTS / CLIENTS BY SALES
                            </div>
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="sales"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="clip-stats"></i>TOTAL BOOKINGS SCHEDULE BY SALES
                            </div>
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="sales2"></div>
                                </figure>
                            </div>
                        </div>
                    </div>-->
                    
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
   
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="https://code.highcharts.com/highcharts.src.js"></script>

    <script src='<?php echo base_url();?>vendor/calendar/fullcalendar/packages/core/main.js'></script>
    <script src='<?php echo base_url();?>vendor/calendar/fullcalendar/packages/interaction/main.js'></script>
    <script src='<?php echo base_url();?>vendor/calendar/fullcalendar/packages/daygrid/main.js'></script>
    <script src='<?php echo base_url();?>vendor/calendar/fullcalendar/packages/timegrid/main.js'></script>
    <script src='<?php echo base_url();?>vendor/calendar/fullcalendar/packages/list/main.js'></script>

    <script src="<?php echo base_url();?>vendor/bower_components/fullcalendar/dist/fullcalendar.js"></script>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    <?php echo $calendar_graph ?>
    <?php echo $sales_contact_graph ?>
    <?php echo $menu_atasbawah; ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
        });
    </script>

  

    

</body>

</html>