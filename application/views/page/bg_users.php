<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['app_name'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/footer-logo-shadow.png" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('page/style'); ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">Users
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i> USERS
                            </div>
                            <div class="panel-body">
                                <div class="row" style="background-color:white;">
                                    <div class="col-md-12 space20 hidden-sm hidden-xs">
                                        <a style="background: #28317a; color: white;" data-toggle="modal" data-target="#modalTambahUsers" class="btn">
                                            <i class="fa fa-plus"></i> ADD USERS PASTICHE CRM
                                        </a>
                                    </div>
                                    <div class="col-md-12 center space20 hidden-md hidden-lg">
                                        <a style="background: #28317a; color: white;" data-toggle="modal" data-target="#modalTambahUsers" class="btn">
                                            <i class="fa fa-plus"></i> ADD USERS PASTICHE CRM
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="100" class="col-to-export">USERNAME</th>
                                                <th width="140" class="col-to-export">FULL NAME / IDENTITY</th>
                                                <th width="140" class="col-to-export center">ROLE PROFILE</th>
                                                <th width="1" class="col-to-export center">STATUS ACCOUNT</th>
                                                <th width="110" class="center">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($data_users->result_array() as $d)
                                                {
                                            ?>
                                            <tr>
                                                <td align=""><b><?php echo $d['usr_username'] ?></b></td>
                                                <td><span style="text-transform: uppercase;">
                                                    <?php echo $d['usr_title'].' <b>'.$d['usr_first_name'].' '.$d['usr_last_name'] ?></b></span>
                                                    <br>
                                                    E-Mail: <?php echo $d['usr_email_address'] ?>
                                                </td>
                                                <td class="center" style="text-transform: uppercase;"><b>
                                                    <?php 
                                                        if($d['usr_security_profile_id_fk'] == 1)
                                                        {
                                                            echo 'Administrator';
                                                        }
                                                        else if($d['usr_security_profile_id_fk'] == 2)
                                                        {
                                                            echo 'Sales';
                                                        }
                                                        else if($d['usr_security_profile_id_fk'] == 3)
                                                        {
                                                            echo 'Europe Viewer';
                                                        }
                                                        else if($d['usr_security_profile_id_fk'] == 4)
                                                        {
                                                            echo 'API User';
                                                        }
                                                    ?></b>
                                                </td>
                                                <td class="center" style="text-transform: uppercase;">
                                                    <?php 
                                                        if($d['usr_active'] == 1)
                                                        {
                                                            echo "<span style='background-color: #29b348;' class='label'>A c t i v e</span>";
                                                        }
                                                        else
                                                        {
                                                            echo "<span style='background-color: #ec536c;' class='label'>Non-act</span>";
                                                        }
                                                    ?>
                                                </td>
                                                <td align="center">
                                                    <a style="background-color: black;" href="<?php echo base_url() ?>index.php/<?php echo $kontroller?>/resetPassword/<?php echo $d['usr_username'] ?>" class="btn btn-sm tooltips" data-placement="top" data-original-title="Reset"><font color="white">Reset Password</font></a>
                                                    <a style="background-color: #44a2d2;" data-toggle="modal" data-target="#modalEditUsers<?php echo $d['usr_user_id_pk'] ?>" class="btn btn-sm tooltips" data-placement="top" data-original-title="Edit"><font color="white"><i class="fa fa-pencil-square-o"></i></font></a>
                                                    <a style="background-color: #ec536c;" href="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/disableUsers/<?php echo $d['usr_user_id_pk'] ?>" class="btn btn-sm tooltips" data-placement="top" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete it?')"><font color="white"><i class="fa fa-trash-o"></i></font></a></td>
                                                
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalTambahUsers; ?>
            <?php echo $modalEditUsers; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    <?php echo $menu_atasbawah; ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

</body>

</html>