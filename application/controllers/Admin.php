<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * WebDev at Moladev.com
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['count_sales_contact']	= $this->web_app_model->get_count_sales_contact();
			$bc['count_sales_booking']	= $this->web_app_model->get_count_sales_booking();

			$bc['usr_first_name'] 		= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['data_booking']			= $this->web_app_model->getAllData('bookings');
			$bc['get_booking_calendar']	= $this->web_app_model->getWhereAllItem('lost','bk_status !=','bookings');
			$bc['data_marketing']		= $this->web_app_model->getAllData('marketing_notes');
			$bc['usr_first_name'] 		= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			$bc['calendar_graph'] 		= $this->load->view('graph/calendar',$bc,true);	
			$bc['sales_contact_graph'] 	= $this->load->view('graph/sales',$bc,true);	
			$this->load->view('page/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}


// END SECTION CONTACT / CLIENT

	public function bg_client()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['field_1']				= $this->uri->segment(3);
			$where_1					= $this->uri->segment(4);
			$where_2					= $this->uri->segment(5);

			//$bc['data_client'] 			= $this->web_app_model->get_join_all_2where_or('contacts','users','co_user_id_fk','usr_user_id_pk',$bc['field_1'],$where_1,$bc['field_1'],$where_2);
			//$bc['data_client'] 			= $this->web_app_model->getAllJoin('co_user_id_fk','usr_user_id_pk','contacts','users');
			$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');

			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditClient'] 		= $this->load->view('page/modalEditClient',$bc,true);
			$bc['modalTambahClient'] 	= $this->load->view('page/modalTambahClient',$bc,true);
			$this->load->view('page/bg_client',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	function data_client(){
        $data 	=	$this->web_app_model->getWhereAllJoin_ajax('co_user_id_fk','usr_user_id_pk','contacts','users','co_active','1');
        echo json_encode($data);
    }

    function data_client_perpage(){
        /*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		
		$this->db->select('*');
		$this->db->join('users', 'contacts.co_user_id_fk = users.usr_user_id_pk');
		$this->db->from('contacts');
		$this->db->where('co_active','1');
		$this->db->order_by('co_contact_id_pk', 'DESC');

		$total = $this->db->get()->num_rows();
		//$total=$this->db->count_all_results("bookings");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("co_company_name",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/

		//$this->db->order_by('bk_date_of_booking','DESC');
		//$query=$this->db->get('bookings');

		$this->db->select('*');
		$this->db->join('users', 'contacts.co_user_id_fk = users.usr_user_id_pk');
		$this->db->from('contacts');
		$this->db->where('co_active','1');
		$this->db->order_by('co_contact_id_pk', 'DESC');


		$query = $this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("co_company_name",$search);
		$jum=$this->db->get('contacts');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$kontroller 			= $this->session->userdata('kontroller');

		$nomor_urut=$start+1;
		foreach ($query->result_array() as $client) {
			$hapus 					= '"Are you sure you want to delete the '.$client['co_company_name'].'?"';

			$output['data'][]=array("<b><p align='center'>".$nomor_urut."</p></b>","<b style='text-transform: uppercase;'>".$client['co_company_name']."</b>".($client['co_company_town'] == '' ? '' : '<br><i class="clip-map"></i> '.$client['co_company_town'].'')." ".($client['co_company_telephone'] == '' || $client['co_contact_mobile'] == '' ? '' : '<br><i class="clip-phone"></i> '.$client['co_company_telephone'].'')." ".($client['co_contact_email'] == '' ? '' : '<br><i class="fa fa-send-o"></i> '.$client['co_contact_email'].'')." ".($client['co_company_website'] == '' ? '' : '<br><i class="clip-globe"></i> '.$client['co_company_website'].'')."","<span style='text-transform: uppercase;'>".$client['usr_title'].". <b>".$client['co_contact_name']."</b></span>".($client['co_contact_position'] == '' ? '' : '<br><i class="fa fa-suitcase"></i>  '.$client['co_contact_position'].'')." ".($client['co_contact_telephone'] == '' || $client['co_contact_mobile'] == '' ? '' : '<br><i class="fa fa-phone"></i>  '.$client['co_contact_telephone'].'/'.$client['co_contact_mobile'].'')." ".($client['co_contact_email'] == '' ? '' : '<br><i class="fa fa-send-o"></i>  '.$client['co_contact_email'].'')."","<span style='text-transform: uppercase;'>".$client['usr_title'].". <b>".$client['usr_first_name']." ".$client['usr_last_name']."</b></span><br><i class='fa fa-suitcase'></i> Sales ".($client['usr_email_address'] == '' ? '' : '<br><i class="fa fa-send-o"></i> '.$client['usr_email_address'].'')."","".($client['co_active'] == '1' ? '<span style="background-color: #29b348;" class="label"> TO BE PARTNERED </span>' : '<span style="background-color: #ec536c;" class="label">NOT TO BE PARTNERED </span>')."","<a style='background-color: #44a2d2;' href='".base_url()."index.php/".$kontroller."/bg_edit_client/".$client['co_contact_id_pk']."?client=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> <a style='background-color: #ec536c;' href='".base_url()."index.php/".$kontroller."/disableContact/".$client['co_contact_id_pk']."' class='btn btn-xs tooltips' data-placement='top' data-original-title='Not To Be Partnered' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i></font></a>");
		$nomor_urut++;
		}

		echo json_encode($output);

    }

    function data_client_perpage_optimized(){
        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order');
		$kontroller = $this->session->userdata('kontroller');

		$this->db->from('contacts')->join('users', 'contacts.co_user_id_fk = users.usr_user_id_pk');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('co_company_name', $value['dir']);
						break;
					case 1:
						$this->db->order_by('co_contact_name', $value['dir']);
						break;
					case 2:
						$this->db->order_by('usr_first_name', $value['dir']);
						break;
					case 3:
						$this->db->order_by('co_active', $value['dir']);
					default:
						break;
				}
			}
		}
			

		if($search!=""){
			$this->db->like("co_contact_name",$search);
			$this->db->or_like("co_company_name",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		foreach ($data as $client) {
			$hapus = '"Are you sure you want to delete the '.$client['co_company_name'].'?"';
			$output['data'][]=array(
				"<b style='text-transform: uppercase;'>".$client['co_company_name']."</b>".($client['co_company_town'] == '' ? '' : '<br><i class="clip-map"></i> '.$client['co_company_town'].'')." ".($client['co_company_telephone'] == '' || $client['co_contact_mobile'] == '' ? '' : '<br><i class="clip-phone"></i> '.$client['co_company_telephone'].'')." ".($client['co_contact_email'] == '' ? '' : '<br><i class="fa fa-send-o"></i> '.$client['co_contact_email'].'')." ".($client['co_company_website'] == '' ? '' : '<br><i class="clip-globe"></i> '.$client['co_company_website'].'')."",
				"<span style='text-transform: uppercase;'>".$client['usr_title'].". <b>".$client['co_contact_name']."</b></span>".($client['co_contact_position'] == '' ? '' : '<br><i class="fa fa-suitcase"></i>  '.$client['co_contact_position'].'')." ".($client['co_contact_telephone'] == '' || $client['co_contact_mobile'] == '' ? '' : '<br><i class="fa fa-phone"></i>  '.$client['co_contact_telephone'].'/'.$client['co_contact_mobile'].'')." ".($client['co_contact_email'] == '' ? '' : '<br><i class="fa fa-send-o"></i>  '.$client['co_contact_email'].'')."",
				"<span style='text-transform: uppercase;'>".$client['usr_title'].". <b>".$client['usr_first_name']." ".$client['usr_last_name']."</b></span><br><i class='fa fa-suitcase'></i> Sales ".($client['usr_email_address'] == '' ? '' : '<br><i class="fa fa-send-o"></i> '.$client['usr_email_address'].'')."",
				"".($client['co_active'] == '1' ? '<span style="background-color: #29b348;" class="label"> TO BE PARTNERED </span>' : '<span style="background-color: #ec536c;" class="label">NOT TO BE PARTNERED </span>')."",
				"<a style='background-color: #44a2d2;' href='".base_url()."index.php/".$kontroller."/bg_edit_client/".$client['co_contact_id_pk']."?client=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> <a style='background-color: #ec536c;' href='".base_url()."index.php/".$kontroller."/disableContact/".$client['co_contact_id_pk']."' class='btn btn-xs tooltips' data-placement='top' data-original-title='Not To Be Partnered' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i></font></a>");
		}

		$output['recordsTotal'] = $this->db->from('contacts')->join('users', 'contacts.co_user_id_fk = users.usr_user_id_pk')->get()->num_rows();

		echo json_encode($output);
    }

    public function bg_edit_client()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config'] = $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_users'] = $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			$bc['data_client'] = $this->web_app_model->getWhereOneItem($this->uri->segment(3),'co_contact_id_pk','contacts');
			
			$bc['usr_first_name'] = $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] = $this->session->userdata('usr_last_name');
			$bc['status'] = $this->session->userdata('stts');
			$bc['kontroller'] = $this->session->userdata('kontroller');
			$bc['atas'] = $this->load->view('page/atas',$bc,true);
			$bc['menu'] = $this->load->view('page/menu',$bc,true);
			$bc['bio'] = $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] = $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_edit_client',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahContact()
	{
		$kontroller 				= $this->session->userdata('kontroller');

		$co_company_name		= $this->input->post('co_company_name');
		$co_company_website		= $this->input->post('co_company_website');
		$co_company_telephone	= $this->input->post('co_company_telephone');
		$co_company_address_line1		= $this->input->post('co_company_address_line1');
		$co_company_address_line2		= $this->input->post('co_company_address_line2');
		$co_company_address_line3		= $this->input->post('co_company_address_line3');
		$co_company_town		= $this->input->post('co_company_town');
		$co_company_postcode			= $this->input->post('co_company_postcode');
		$co_mailshot_sent			= $this->input->post('co_mailshot_sent');
		$co_contacted			= $this->input->post('co_contacted');
		$co_quoted				= $this->input->post('co_quoted');
		$co_prospect			= $this->input->post('co_prospect');
		$co_client				= $this->input->post('co_client');
		$co_nightclub			= $this->input->post('co_nightclub');
		$co_contact_name		= $this->input->post('co_contact_name');
		$co_contact_position			= $this->input->post('co_contact_position');
		$co_contact_telephone	= $this->input->post('co_contact_telephone');
		$co_contact_mobile		= $this->input->post('co_contact_mobile');
		$co_contact_email		= $this->input->post('co_contact_email');
		$co_user_id_fk		= $this->input->post('co_user_id_fk');
		$co_notes				= $this->input->post('co_notes');

			$data = array(		
				'co_company_name' 		=> $co_company_name,
				'co_company_website'	=> $co_company_website,
				'co_company_telephone'	=> $co_company_telephone,
				'co_company_address_line1'		=> $co_company_address_line1,
				'co_company_address_line2'		=> $co_company_address_line2,
				'co_company_address_line3'		=> $co_company_address_line3,
				'co_company_town'		=> $co_company_town,
				'co_company_postcode'			=> $co_company_postcode,
				'co_mailshot_sent'			=> $co_mailshot_sent,
				'co_contacted'			=> $co_contacted,
				'co_quoted'				=> $co_quoted,
				'co_prospect'			=> $co_prospect,
				'co_client'				=> $co_client,
				'co_nightclub'			=> $co_nightclub,
				'co_contact_name'		=> $co_contact_name,
				'co_contact_position'			=> $co_contact_position,
				'co_contact_telephone'	=> $co_contact_telephone,
				'co_contact_mobile'		=> $co_contact_mobile,
				'co_contact_email'		=> $co_contact_email,
				'co_user_id_fk'	=> $co_user_id_fk,
				'co_notes'				=> $co_notes,
				);
			
			$this->web_app_model->insertData($data,'contacts');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_client?client=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Contact has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Contact has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapusContact()
	{
		$kontroller 	= $this->session->userdata('kontroller');
		$id 			= $this->uri->segment(3);
		$hapus 			= array('co_contact_id_pk'=>$id);


		$this->web_app_model->deleteData('contacts',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_client?client=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Contact has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Contact has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function editContact()
	{
		$kontroller 				= $this->session->userdata('kontroller');

		$co_contact_id_pk				= $this->input->post('co_contact_id_pk');
		$co_company_name		= $this->input->post('co_company_name');
		$co_company_website		= $this->input->post('co_company_website');
		$co_company_telephone	= $this->input->post('co_company_telephone');
		$co_company_address_line1		= $this->input->post('co_company_address_line1');
		$co_company_address_line2		= $this->input->post('co_company_address_line2');
		$co_company_address_line3		= $this->input->post('co_company_address_line3');
		$co_company_town		= $this->input->post('co_company_town');
		$co_company_postcode			= $this->input->post('co_company_postcode');
		$co_mailshot_sent			= $this->input->post('co_mailshot_sent');
		$co_contacted			= $this->input->post('co_contacted');
		$co_quoted				= $this->input->post('co_quoted');
		$co_prospect			= $this->input->post('co_prospect');
		$co_client				= $this->input->post('co_client');
		$co_nightclub			= $this->input->post('co_nightclub');
		$co_contact_name		= $this->input->post('co_contact_name');
		$co_contact_position			= $this->input->post('co_contact_position');
		$co_contact_telephone	= $this->input->post('co_contact_telephone');
		$co_contact_mobile		= $this->input->post('co_contact_mobile');
		$co_contact_email		= $this->input->post('co_contact_email');
		$co_user_id_fk		= $this->input->post('co_user_id_fk');
		$co_notes				= $this->input->post('co_notes');

		$data = array(		
			'co_company_name' 		=> $co_company_name,
			'co_company_website' 	=> $co_company_website,
			'co_company_telephone' 	=> $co_company_telephone,
			'co_company_address_line1' 	=> $co_company_address_line1,
			'co_company_address_line2' 	=> $co_company_address_line2,
			'co_company_address_line3' 	=> $co_company_address_line3,
			'co_company_town' 		=> $co_company_town,
			'co_company_postcode' 			=> $co_company_postcode,
			'co_mailshot_sent' 			=> $co_mailshot_sent,
			'co_contacted' 			=> $co_contacted,
			'co_quoted' 			=> $co_quoted,
			'co_prospect' 			=> $co_prospect,
			'co_client' 			=> $co_client,
			'co_nightclub' 			=> $co_nightclub,
			'co_contact_name' 		=> $co_contact_name,
			'co_contact_position' 			=> $co_contact_position,
			'co_contact_telephone' 	=> $co_contact_telephone,
			'co_contact_mobile' 	=> $co_contact_mobile,
			'co_contact_email' 		=> $co_contact_email,
			'co_user_id_fk' 	=> $co_user_id_fk,
			'co_notes' 				=> $co_notes,
			);

		$where = array(		
			'co_contact_id_pk' 		=> $co_contact_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'contacts');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_client?client=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Contact has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data users has been edited!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function updateConfig()
	{
		$kontroller 			= $this->session->userdata('kontroller');
	
		$versi					= $this->input->post('versi');
		$app_name				= $this->input->post('app_name');
		$email_sender			= $this->input->post('email_sender');
		$email_sender_pass		= $this->input->post('email_sender_pass');
		$email_reciever			= $this->input->post('email_reciever');
		
		$data = array(		
			'versi' 			=> $versi,
			'app_name' 			=> $app_name,
			'email_sender' 		=> $email_sender,
			'email_sender_pass' => $email_sender_pass,
			'email_reciever' 	=> $email_reciever,
			);

		$where = array(		
			'id_config' 		=> 1,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_config');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_config?config=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Configuration has been updated!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Config has been updated!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function disableContact()
	{
		$kontroller 				= $this->session->userdata('kontroller');

		$co_contact_id_pk				= $this->uri->segment(3);

		$data = array(		
			'co_active' 		=> '0',
			);

		$where = array(		
			'co_contact_id_pk' 		=> $co_contact_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'contacts');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_client?client=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Contact has been disabled!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data contacs has been disabled!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

// START SECTION CONTACT / CLIENT


	// END SECTION MARKETING

	public function bg_marketing()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_marketing']		= $this->web_app_model->getAllData('marketing_notes');

			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			$bc['modalEditMarketing'] 	= $this->load->view('page/modalEditMarketing',$bc,true);
			$bc['modalTambahMarketing']	= $this->load->view('page/modalTambahMarketing',$bc,true);
			$this->load->view('page/bg_marketing',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahMarketing()
	{
		$kontroller 		= $this->session->userdata('kontroller');
		$m_date				= $this->input->post('m_date');
		$m_note				= $this->input->post('m_note');

			$data = array(		
				'm_date' 	=> $m_date,
				'm_note'	=> $m_note,
				);
			
			$this->web_app_model->insertData($data,'marketing_notes');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_marketing?marketing=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Schedule has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Schedule has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapusMarketing()
	{
		$kontroller 	= $this->session->userdata('kontroller');
		$id 			= $this->uri->segment(3);
		$hapus 			= array('m_id'=>$id);


		$this->web_app_model->deleteData('marketing_notes',$hapus);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_marketing?marketing=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Schedule has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Schedule has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function editMarketing()
	{
		$kontroller 		= $this->session->userdata('kontroller');
		$m_id				= $this->input->post('m_id');
		$m_date				= $this->input->post('m_date');
		$m_note				= $this->input->post('m_note');

		$data = array(		
				'm_date' 	=> $m_date,
				'm_note'	=> $m_note,
				);

		$where = array(		
			'm_id' 		=> $m_id,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'marketing_notes');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_marketing?marketing=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Schedule has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Schedule has been edited!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

// START SECTION MARKETING


// END SECTION ARTIST
	public function bg_artist()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			//$bc['data_artist']			= $this->web_app_model->getAllData('artistes');

			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditArtist'] 		= $this->load->view('page/modalEditArtist',$bc,true);
			$bc['modalTambahArtist'] 	= $this->load->view('page/modalTambahArtist',$bc,true);
			$this->load->view('page/bg_artist',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	/*
	function data_artist(){
        $data 	=	$this->web_app_model->getAllData_ajax('artistes');
        echo json_encode($data);
    }
	*/

	function data_artist(){
        $data 	=	$this->web_app_model->getWhereAllItem_ajax('1','ar_active','artistes');
        echo json_encode($data);
    }

    public function bg_edit_artist()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			//$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			$bc['data_artist'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'ar_artiste_id_pk','artistes');
			
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_edit_artist',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahArtist()
	{
		$kontroller 		= $this->session->userdata('kontroller');

		$ar_act_name		= $this->input->post('ar_act_name');
		$ar_website		= $this->input->post('ar_website');
		$ar_entertainment_type	= $this->input->post('ar_entertainment_type');
		$ar_fee			= $this->input->post('ar_fee');
		$ar_band			= $this->input->post('ar_band');
		$ar_band_genre			= $this->input->post('ar_band_genre');
		$ar_portfolio_url	= $this->input->post('ar_portfolio_url');
		$ar_address_line1	= $this->input->post('ar_address_line1');
		$ar_address_line2	= $this->input->post('ar_address_line2');
		$ar_address_line3	= $this->input->post('ar_address_line3');
		$ar_county		= $this->input->post('ar_county');
		$ar_postcode		= $this->input->post('ar_postcode');
		$ar_contact_name	= $this->input->post('ar_contact_name');
		$ar_telephone		= $this->input->post('ar_telephone');
		$ar_email			= $this->input->post('ar_email');
		$ar_location		= $this->input->post('ar_location');
		$ar_notes			= $this->input->post('ar_notes');

			$data = array(		
				'ar_act_name' 		=> $ar_act_name,
				'ar_website'		=> $ar_website,
				'ar_entertainment_type'	=> $ar_entertainment_type,
				'ar_fee'			=> $ar_fee,
				'ar_band'			=> $ar_band,
				'ar_band_genre'			=> $ar_band_genre,
				'ar_portfolio_url'=> $ar_portfolio_url,
				'ar_address_line1'=> $ar_address_line1,
				'ar_address_line2'=> $ar_address_line2,
				'ar_address_line3'=> $ar_address_line3,
				'ar_county'		=> $ar_county,
				'ar_postcode'		=> $ar_postcode,
				'ar_contact_name'	=> $ar_contact_name,
				'ar_telephone'		=> $ar_telephone,
				'ar_email'			=> $ar_email,
				'ar_location'		=> $ar_location,
				'ar_notes'			=> $ar_notes,
				);
			
			$this->web_app_model->insertData($data,'artistes');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist/?artist=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Artist has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Artist has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapusArtist()
	{
		$kontroller 	= $this->session->userdata('kontroller');
		$id 			= $this->uri->segment(3);
		$hapus 			= array('ar_artiste_id_pk'=>$id);


		$this->web_app_model->deleteData('artistes',$hapus);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist?artist=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Artist has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Artist has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function editArtist()
	{
		$kontroller 		= $this->session->userdata('kontroller');
		$ar_artiste_id_pk			= $this->input->post('ar_artiste_id_pk');
		$ar_act_name		= $this->input->post('ar_act_name');
		$ar_website		= $this->input->post('ar_website');
		$ar_entertainment_type	= $this->input->post('ar_entertainment_type');
		$ar_fee			= $this->input->post('ar_fee');
		$ar_band			= $this->input->post('ar_band');
		$ar_band_genre			= $this->input->post('ar_band_genre');
		$ar_portfolio_url	= $this->input->post('ar_portfolio_url');
		$ar_address_line1	= $this->input->post('ar_address_line1');
		$ar_address_line2	= $this->input->post('ar_address_line2');
		$ar_address_line3	= $this->input->post('ar_address_line3');
		$ar_county		= $this->input->post('ar_county');
		$ar_postcode		= $this->input->post('ar_postcode');
		$ar_contact_name	= $this->input->post('ar_contact_name');
		$ar_telephone		= $this->input->post('ar_telephone');
		$ar_email			= $this->input->post('ar_email');
		$ar_location		= $this->input->post('ar_location');
		$ar_notes			= $this->input->post('ar_notes');

		$data = array(		
				'ar_act_name' 		=> $ar_act_name,
				'ar_website'		=> $ar_website,
				'ar_entertainment_type'	=> $ar_entertainment_type,
				'ar_fee'			=> $ar_fee,
				'ar_band'			=> $ar_band,
				'ar_band_genre'			=> $ar_band_genre,
				'ar_portfolio_url'=> $ar_portfolio_url,
				'ar_address_line1'=> $ar_address_line1,
				'ar_address_line2'=> $ar_address_line2,
				'ar_address_line3'=> $ar_address_line3,
				'ar_county'		=> $ar_county,
				'ar_postcode'		=> $ar_postcode,
				'ar_contact_name'	=> $ar_contact_name,
				'ar_telephone'		=> $ar_telephone,
				'ar_email'			=> $ar_email,
				'ar_location'		=> $ar_location,
				'ar_notes'			=> $ar_notes,
				);

		$where = array(		
			'ar_artiste_id_pk' 				=> $ar_artiste_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'artistes');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist?artist=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Artist has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Artist has been edited!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function disableArtist()
	{
		$kontroller 			= $this->session->userdata('kontroller');
		$ar_artiste_id_pk 				= $this->uri->segment(3);

		$data = array(		
				'ar_active'	=> '0',
				);

		$where = array(		
			'ar_artiste_id_pk' 				=> $ar_artiste_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'artistes');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist?artist=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Artist has been disabled!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Artist has been disabled!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}
// START SECTION ARTIST 


// SECTION USERS - START

	public function bg_users()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_users'] 			= $this->web_app_model->getWhereAllItem('1','usr_active','users');

			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			$bc['modalTambahUsers'] 	= $this->load->view('page/modalTambahUsers',$bc,true);
			$this->load->view('page/bg_users',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahUsers()
	{
		$kontroller 				= $this->session->userdata('kontroller');
		$usr_title				= $this->input->post('usr_title');
		$usr_username				= $this->input->post('usr_username');
		$usr_first_name			= $this->input->post('usr_first_name');
		$usr_last_name			= $this->input->post('usr_last_name');
		$usr_email_address				= $this->input->post('usr_email_address');
		$usr_security_profile_id_fk					= $this->input->post('usr_security_profile_id_fk');
		$usr_active				= '1';

		$cek_username		 		= $this->web_app_model->cek_username('usr_username',$usr_username);

		if($cek_username['total'] == 0)
		{

			$data = array(		
				//'usr_user_id_pk' 			=> $usr_user_id_pk, auto
				'usr_title' 			=> $usr_title,
				'usr_username'		=> $usr_username,
				'usr_password'	=> md5($usr_username),
				'usr_first_name'		=> $usr_first_name,
				'usr_last_name'		=> $usr_last_name,
				'usr_email_address'			=> $usr_email_address,
				'usr_security_profile_id_fk'			=> $usr_security_profile_id_fk,
				'usr_active'			=> $usr_active,
				);
			
			$this->web_app_model->insertData($data,'users');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														User Data has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'User Data has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");

			// CONFIG ============ Wajib Ada
				$data_config			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
				// END CONFIG ======== Wajib Ada

				// NOTIFIKASI BY EMAIL

				require("vendor/PHPMailer-master/src/PHPMailer.php");
				require("vendor/PHPMailer-master/src/SMTP.php");
				require("vendor/PHPMailer-master/src/Exception.php");
				require("vendor/PHPMailer-master/src/OAuth.php");
				    
				$message = '
				    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				</head>

				<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
				<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
				Your account has been registered to the Pastiche CRM system (Diary Pastiche) with the following details:    <br><br>
				        <div style="float:left; width:150px; margin-bottom:5px;">Full Name  :</div>
				        <div style="float:left;"><strong>'.$usr_title.'. '.$usr_first_name.' '.$usr_last_name.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Username  :</div>
				        <div style="float:left;"><strong>'.$usr_username.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Password:</div>
				        <div style="float:left;"><strong>'.$usr_username.'</strong></div>
				        <div style="clear:both"></div><br><br>
				 Please login and change your password for the security of your account
				 <td><tr></table>
				 <br><br><br><a href="'.base_url().'index.php"><b>==> Login to Diary Pastiche <==</b></a>
				</body>
				</html>';

				  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
				//$mail = new PHPMailer;
				$mail->IsSMTP();
				$mail->SMTPSecure = 'ssl';
				$mail->Host = "mail.pastichestudios.co.uk"; //host masing2 provider email
				$mail->SMTPDebug = 1;
				$mail->Port = 465;
				$mail->SMTPAuth = true;
				$mail->IsHTML(true);
				$mail->Username = "".$data_config['email_sender'].""; //user email yang sebelumnya anda buat
				$mail->Password = "".$data_config['email_sender_pass'].""; //password email yang sebelumnya anda buat
				$mail->SetFrom("".$data_config['email_sender']."","Pastiche Diary"); //set email pengirim
				$mail->Subject = "Pastiche Diary - Your account has been registered!"; //subyek email
				$mail->addAddress("".$usr_email_address."","Pastiche Management");  //tujuan email
				$mail->MsgHTML($message);
				$mail->Send();

		}
		else
		{

			header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-info'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Opps - 
														</strong>
														Username already exists!, please try again.
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Oops!!',
											                text:  'Username already exists, please try again!',
											                type: 'info',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
	}

	public function editUsers()
	{
		$kontroller 		= $this->session->userdata('kontroller');
		$usr_user_id_pk			= $this->input->post('usr_user_id_pk');
		$usr_title		= $this->input->post('usr_title');
		$usr_username		= $this->input->post('usr_username');
		$usr_first_name	= $this->input->post('usr_first_name');
		$usr_last_name	= $this->input->post('usr_last_name');
		$usr_email_address		= $this->input->post('usr_email_address');
		$usr_security_profile_id_fk			= $this->input->post('usr_security_profile_id_fk');
		$usr_active		= '1';

		$data = array(		
			'usr_username' 	=> $usr_username,
			'usr_title' 		=> $usr_title,
			'usr_first_name' 	=> $usr_first_name,
			'usr_last_name' 	=> $usr_last_name,
			'usr_email_address' 		=> $usr_email_address,
			'usr_security_profile_id_fk' 		=> $usr_security_profile_id_fk,
			'usr_active' 		=> $usr_active,
			);

		$where = array(		
			'usr_user_id_pk' 		=> $usr_user_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'users');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data users has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data users has been edited!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}	

	public function hapusUsers()
	{
		$kontroller 	= $this->session->userdata('kontroller');
		$id 			= $this->uri->segment(3);
		$hapus 			= array('usr_user_id_pk'=>$id);


		$this->web_app_model->deleteData('users',$hapus);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										User has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'User has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function disableUsers()
	{
		$kontroller 				= $this->session->userdata('kontroller');
		$usr_user_id_pk				= $this->uri->segment(3);

		$data = array(		
			'usr_active' 		=> '0',
			);

		$where = array(		
			'usr_user_id_pk' 		=> $usr_user_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'users');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data User has been disabled!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data user has been disabled!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

// SECTION USERS - END


// SECTION USERS - START

	public function bg_artist_booking()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_booking'] 		= $this->web_app_model->get_booking_1('bk_booking_id_pk',$this->uri->segment(3));
			$bc['data_artist']			= $this->web_app_model->getWhereAllItem('1','ar_active','artistes');
			$bc['data_artist_booking']	= $this->web_app_model->getWhereAllJoin('ab_artiste_id_fk','ar_artiste_id_pk','artiste_bookings','artistes','ab_booking_id_fk',$this->uri->segment(3));

			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			$bc['modalEditArtistBooking'] 		= $this->load->view('page/modalEditArtistBooking',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_artist_booking',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function add_artist_booking()
	{
		$kontroller 				= $this->session->userdata('kontroller');
		$ab_booking_id_fk			= $this->input->post('ab_booking_id_fk');
		$ab_artiste_id_fk			= $this->input->post('ab_artiste_id_fk');

		$cek_artist_fee				= $this->web_app_model->getWhereOneItem($ab_artiste_id_fk,'ar_artiste_id_pk','artistes');
		$ab_booking_fee				= $this->input->post('ar_fee');
		$ab_paid					= $this->input->post('ab_paid');

		$cek_artist_booking			= $this->web_app_model->cek_artist_booking('ab_artiste_id_fk', $ab_artiste_id_fk, 'ab_booking_id_fk', $ab_booking_id_fk);

		if($cek_artist_booking['total'] == 0)
		{
			$data = array(		
				'ab_booking_id_fk' 		=> $ab_booking_id_fk,
				'ab_artiste_id_fk' 		=> $ab_artiste_id_fk,
				'ab_booking_fee'		=> $ab_booking_fee,
				'ab_paid'				=> $ab_paid,
				);
			
			$this->web_app_model->insertData($data,'artiste_bookings');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist_booking/'.$ab_booking_id_fk.'?booking=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Artist Booking has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Artist Booking has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");	
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_artist_booking/'.$ab_booking_id_fk.'?booking=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Oops! - 
														</strong>
														Artist already exists!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Oops!!',
											                text:  'Artist already exists!',
											                type: 'info',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");	
		}
		
	}

	public function editArtistBooking()
	{
		$kontroller 				= $this->session->userdata('kontroller');
		$ab_no						= $this->input->post('ab_no');
		$ab_booking_fee				= $this->input->post('ab_booking_fee');
		$bk_booking_id_pk			= $this->input->post('bk_booking_id_pk');
		$ab_paid					= $this->input->post('ab_paid');

		$data = array(		
				'ab_booking_fee' 	=> $ab_booking_fee,
				'ab_paid' 			=> $ab_paid,
				);

		$where = array(		
			'ab_no' 				=> $ab_no,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'artiste_bookings');
		//header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist_booking/'.$bk_booking_id_pk.'?booking=1/');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_editBooking/'.$bk_booking_id_pk.'?booking=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Artist Booking has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Artist Booking has been edited!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}	

	public function hapusArtistBooking()
	{
		$kontroller 		= $this->session->userdata('kontroller');
		$id 				= $this->uri->segment(3);
		$bk_booking_id_pk 	= $this->uri->segment(4);
		$hapus 			= array('ab_no'=>$id);


		$this->web_app_model->deleteData('artiste_bookings',$hapus);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist_booking/'.$bk_booking_id_pk.'?booking=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Artist Booking been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Artist Booking has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function bg_booking()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			if(@$_GET['stts1'] == 'in%20progress')
			{
				$stts1 = 'in progress';
			}
			else if(@$_GET['stts2'] == 'in%20progress')
			{
				$stts2 = 'in progress';
			}
			else
			{
				$stts1 = @$_GET['stts1'];
				$stts2 = @$_GET['stts2'];
			}

			//$bc['data_booking'] 		= $this->web_app_model->get_booking($this->uri->segment(3), $this->uri->segment(3), $stts1, $stts2);
			//$bc['data_booking'] 		= $this->web_app_model->get_booking_all();
			$bc['data_users']			= $this->web_app_model->getWhereAllItem('2','usr_security_profile_id_fk','users');
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_booking',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	function data_booking(){
        $data=$this->web_app_model->get_booking_all();
        echo json_encode($data);
    }

	function data_booking_perpage(){
        /*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total didalam database*/
		$this->db->select('*');
		$this->db->where("bk_date_of_booking BETWEEN date('2018-11-17') AND date('".date('Y-m-d')."')");
		$this->db->from('bookings');
		$this->db->join('users', 'users.usr_user_id_pk = bookings.bk_user_id_fk');
		$this->db->join('contacts', 'contacts.co_contact_id_pk = bookings.bk_contact_id_fk');
		$this->db->order_by('bk_date_of_booking', 'DESC');

		$total = $this->db->get()->num_rows();
		//$total=$this->db->count_all_results("bookings");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){

			$this->db->like("bk_event_name",$search);
			$this->db->or_like("co_company_name",$search);
			$this->db->where("bk_date_of_booking BETWEEN date('2018-11-17') AND date('".date('Y-m-d')."')");
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/

		//$this->db->order_by('bk_date_of_booking','DESC');
		//$query=$this->db->get('bookings');

		$this->db->select('*');
		$this->db->where("bk_date_of_booking BETWEEN date('2018-11-17') AND date('".date('Y-m-d')."')");
		$this->db->from('bookings');
		$this->db->join('users', 'users.usr_user_id_pk = bookings.bk_user_id_fk');
		$this->db->join('contacts', 'contacts.co_contact_id_pk = bookings.bk_contact_id_fk');
		$this->db->order_by('bk_date_of_booking', 'DESC');

		$query = $this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){

			$this->db->like("bk_event_name",$search);
			$this->db->or_like("co_company_name",$search);
			$this->db->where("bk_date_of_booking BETWEEN date('2018-11-17') AND date('".date('Y-m-d')."')");
			$this->db->join('contacts', 'contacts.co_contact_id_pk = bookings.bk_contact_id_fk');
		
			$jum=$this->db->get('bookings');
			$output['recordsTotal'] = $output['recordsFiltered'] = $jum->num_rows();
		}

		$this->db->limit($length,$start);




		$kontroller = $this->session->userdata('kontroller');

		$nomor_urut=$start+1;
		foreach ($query->result_array() as $booking) {
			$hapus 					= '"Are you sure you want to delete the '.$booking['bk_event_name'].' event?"';
			$duplicate 				= '"Are you sure you want to duplicate the '.$booking['bk_event_name'].' event?"';

			$output['data'][]=array("<b><p align='center'>".$nomor_urut."</p></b>",
				date('d / M / Y', strtotime($booking['bk_date_of_booking'])),$booking['co_company_name'],"<b>".$booking['bk_event_name']."</b>".($booking['bk_venue'] == '' ? '' : '<br><i class="clip-location"></i> '.$booking['bk_venue'].''),
				"<b>".date('d / M / Y', strtotime($booking['bk_date_of_event']))."</b>".($booking['bk_time_of_event'] == '' ? '' : '<br><i class="clip-clock"></i> '.$booking['bk_time_of_event'].'')."",$booking['usr_first_name']." ".$booking['usr_last_name'],
				"<a target='_blank' style='background-color: #44a2d2;' href='".base_url()."index.php/".$kontroller."/bg_editBooking/".$booking['bk_booking_id_pk']."?booking=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> <a style='background-color: #ec536c;' href=".base_url()."index.php/".$kontroller."/hapusBooking/".$booking['bk_booking_id_pk']." class='btn btn-xs tooltips' data-placement='top' data-original-title='Delete' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash-o'></i></font></a> <a style='background-color: #008080;' href=".base_url()."index.php/".$kontroller."/duplicate_booking/".$booking['bk_booking_id_pk']." class='btn btn-xs tooltips' data-placement='top' data-original-title='Delete' onclick='return confirm(".$duplicate.")'><font color='white'><i class='fa clip-file-plus'></i></font></a>");
		$nomor_urut++;
		}

		echo json_encode($output);

    }
	
	function data_booking_perpage_optimized(){
        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order');
		$kontroller = $this->session->userdata('kontroller');

		$this->db->from('bookings')
			->join('users', 'users.usr_user_id_pk = bookings.bk_user_id_fk')
			->join('contacts', 'contacts.co_contact_id_pk = bookings.bk_contact_id_fk');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('bk_date_of_booking', $value['dir']);
						break;
					case 1:
						$this->db->order_by('co_company_name', $value['dir']);
						break;
					case 2:
						$this->db->order_by('bk_event_name', $value['dir']);
						break;
					case 3:
						$this->db->order_by('bk_date_of_event', $value['dir']);
						break;
					case 4:
						$this->db->order_by('usr_first_name', $value['dir']);
						break;
					default:
						break;
				}
			}
		}
			

		if($search!=""){
			$this->db->like("bk_event_name",$search);
			$this->db->or_like("co_company_name",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		foreach ($data as $booking) {
			$hapus 					= '"Are you sure you want to delete the '.$booking['bk_event_name'].' event?"';
			$duplicate 				= '"Are you sure you want to duplicate the '.$booking['bk_event_name'].' event?"';

			$output['data'][]=array(
				date('d / M / Y', strtotime($booking['bk_date_of_booking'])),
				$booking['co_company_name'],
				"<b>".$booking['bk_event_name']."</b>".($booking['bk_venue'] == '' ? '' : '<br><i class="clip-location"></i> '.$booking['bk_venue'].''),
				"<b>".date('d / M / Y', strtotime($booking['bk_date_of_event']))."</b>".($booking['bk_time_of_event'] == '' ? '' : '<br><i class="clip-clock"></i> '.$booking['bk_time_of_event'].'')."",
				$booking['usr_first_name']." ".$booking['usr_last_name'],
				"<a target='_blank' style='background-color: #44a2d2;' href='".base_url()."index.php/".$kontroller."/bg_editBooking/".$booking['bk_booking_id_pk']."?booking=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> <a style='background-color: #ec536c;' href=".base_url()."index.php/".$kontroller."/hapusBooking/".$booking['bk_booking_id_pk']." class='btn btn-xs tooltips' data-placement='top' data-original-title='Delete' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash-o'></i></font></a> <a style='background-color: #008080;' href=".base_url()."index.php/".$kontroller."/duplicate_booking/".$booking['bk_booking_id_pk']." class='btn btn-xs tooltips' data-placement='top' data-original-title='Delete' onclick='return confirm(".$duplicate.")'><font color='white'><i class='fa clip-file-plus'></i></font></a>"
			);
		}

		$output['recordsTotal'] = $this->db->from('bookings')
											->join('users', 'users.usr_user_id_pk = bookings.bk_user_id_fk')
											->join('contacts', 'contacts.co_contact_id_pk = bookings.bk_contact_id_fk')
											->get()->num_rows();

		echo json_encode($output);
    }  

	public function bg_tambahBooking()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['last_id']				= $this->get_last_id_increament('pastiche_crm','bookings');
			$bc['data_artist']			= $this->web_app_model->getWhereAllItem('1','ar_active','artistes');
			//$bc['data_artist_booking']	= $this->web_app_model->getWhereAllJoin('ab_artiste_id_fk','ar_artiste_id_pk','artiste_bookings','artistes','ab_booking_id_fk','10238');

			
			// FOR CEK ID ARTIST YG TIDAK TERPAKAI DALAM BOOKING
			$last_id				= $this->get_last_id_increament('pastiche_crm','bookings');
			$cek_last_id			= $this->web_app_model->getWhereOneItem($last_id,'bk_booking_id_pk','bookings');

			if(empty($cek_last_id))
			{
				$hapus 			= array('ab_booking_id_fk'=>$last_id);

				$this->web_app_model->deleteData('artiste_bookings',$hapus);
			}
			// END


			//$bc['data_booking'] 		= $this->web_app_model->get_booking($this->uri->segment(3), $this->uri->segment(3), '1', 'Y');
			$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			$bc['data_client']			= $this->web_app_model->getWhereAllItem('1','co_active','contacts');
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			$bc['modalTambahArtistBooking'] 	= $this->load->view('page/modalTambahArtistBooking',$bc,true);
			$this->load->view('page/bg_tambahBooking',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_editBooking()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_artist']			= $this->web_app_model->getWhereAllItem('1','ar_active','artistes');

			$bc['data_booking'] 		= $this->web_app_model->get_booking_1('bk_booking_id_pk',$this->uri->segment(3));
			$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			$bc['data_client']			= $this->web_app_model->getWhereAllItem('1','co_active','contacts');
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			$bc['data_artist_booking']	= $this->web_app_model->getWhereAllJoin('ab_artiste_id_fk','ar_artiste_id_pk','artiste_bookings','artistes','ab_booking_id_fk',$this->uri->segment(3));
			$bc['modalEditArtistBooking'] 		= $this->load->view('page/modalEditArtistBooking',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_editBooking',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_config()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			//$bc['data_booking'] 		= $this->web_app_model->get_booking_1('bk_booking_id_pk',$this->uri->segment(3));
			//$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			//$bc['data_client']			= $this->web_app_model->getWhereAllItem('1','co_active','contacts');
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_config',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahBooking()
	{
		$kontroller				= $this->session->userdata('kontroller');
		
		$bk_event_name			= $this->input->post('bk_event_name');
		$bk_venue				= $this->input->post('bk_venue');
		$bk_date_of_event		= $this->input->post('bk_date_of_event');
		$bk_time_of_event		= $this->input->post('bk_time_of_event');
		$bk_date_of_booking		= $this->input->post('bk_date_of_booking');
		$bk_forecast			= $this->input->post('bk_forecast');
		$bk_price				= $this->input->post('bk_price');

		$bk_cost				= $this->input->post('bk_cost');
		$bk_profit				= $this->input->post('bk_profit');
		$bk_user_id_fk			= $this->input->post('bk_user_id_fk');
		$bk_status				= $this->input->post('bk_status');
		$bk_commission_only		= $this->input->post('bk_commission_only');
		$bk_contract_required	= $this->input->post('bk_contract_required');
		$bk_invoice_required	= $this->input->post('bk_invoice_required');
		$bk_website_enquiry		= 0;
		$bk_european_booking	= 0;
		$bk_quality_lead		= $this->input->post('bk_quality_lead');
		$bk_contact_id_fk		= $this->input->post('bk_contact_id_fk');
		$bk_po_heading			= $this->input->post('bk_po_heading');
		$bk_payment_terms		= $this->input->post('bk_payment_terms');
		$bk_lead_notes			= $this->input->post('bk_lead_notes');
		$bk_rota				= $this->input->post('bk_rota');
		$bk_notes				= $this->input->post('bk_notes');

			$data = array(		
				'bk_event_name' 		=> $bk_event_name,
				'bk_venue' 				=> $bk_venue,
				'bk_date_of_event'		=> $bk_date_of_event,
				'bk_time_of_event'		=> $bk_time_of_event,
				'bk_date_of_booking'	=> $bk_date_of_booking,
				'bk_forecast'			=> $bk_forecast,
				'bk_price'				=> $bk_price,
				'bk_cost'				=> $bk_cost,
				'bk_profit'				=> $bk_profit,
				'bk_user_id_fk'			=> $bk_user_id_fk,
				'bk_status'				=> $bk_status,
				'bk_commission_only'	=> $bk_commission_only,
				'bk_contract_required'	=> $bk_contract_required,
				'bk_invoice_required'	=> $bk_invoice_required,
				'bk_website_enquiry'	=> $bk_website_enquiry,
				'bk_european_booking'	=> $bk_european_booking,
				'bk_quality_lead'		=> $bk_quality_lead,
				'bk_contact_id_fk'		=> $bk_contact_id_fk,
				'bk_po_heading'			=> $bk_po_heading,
				'bk_payment_terms'		=> $bk_payment_terms,
				'bk_lead_notes'			=> $bk_lead_notes,
				'bk_rota'				=> $bk_rota,
				'bk_notes'				=> $bk_notes,
				);



			
			$this->web_app_model->insertData($data,'bookings');

			$last_id = $this->web_app_model->getMaxOneData('bk_booking_id_pk','bookings');

			//header('location:'.base_url().'index.php/'.$kontroller.'/bg_artist_booking/'.$last_id['bk_booking_id_pk'].'?booking=1');
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_booking?booking=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Booking Schedule has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Booking Schedule has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");		


				// CONFIG ============ Wajib Ada
				$data_config			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
				// END CONFIG ======== Wajib Ada

				// NOTIFIKASI BY EMAIL

				require("vendor/PHPMailer-master/src/PHPMailer.php");
				require("vendor/PHPMailer-master/src/SMTP.php");
				require("vendor/PHPMailer-master/src/Exception.php");
				require("vendor/PHPMailer-master/src/OAuth.php");
				    
				$message = '
				    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				</head>

				<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
				<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
				NOTICE : BOOKING HAS BEEN ADDED!    <br><br>
				        <div style="float:left; width:150px; margin-bottom:5px;">Event Name  :</div>
				        <div style="float:left;"><strong>'.$bk_event_name.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Date of event  :</div>
				        <div style="float:left;"><strong>'.$bk_date_of_event.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Date of booking:</div>
				        <div style="float:left;"><strong>'.$bk_date_of_booking.'</strong></div>
				        <div style="clear:both"></div>

				        <div style="float:left; width:150px; margin-bottom:5px;">===============</div>
				        <div style="float:left;"><strong>===============</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">-</div>
				        <div style="float:left;"><strong>-</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Status:</div>
				        <div style="float:left;"><strong>'.$bk_status.'</strong></div>
				        <div style="clear:both"></div>
				 <td><tr></table>
				 <br><br><br><a href="'.base_url().'index.php"><b>==> Go to Diary Pastiche <==</b></a>
				</body>
				</html>';

				  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
				//$mail = new PHPMailer;
				$mail->IsSMTP();
				$mail->SMTPSecure = 'ssl';
				$mail->Host = "mail.pastichestudios.co.uk"; //host masing2 provider email
				$mail->SMTPDebug = 1;
				$mail->Port = 465;
				$mail->SMTPAuth = true;
				$mail->IsHTML(true);
				$mail->Username = "".$data_config['email_sender'].""; //user email yang sebelumnya anda buat
				$mail->Password = "".$data_config['email_sender_pass'].""; //password email yang sebelumnya anda buat
				$mail->SetFrom("".$data_config['email_sender']."","Pastiche Diary"); //set email pengirim
				$mail->Subject = "Pastiche Diary - Booking Has Been Added!"; //subyek email
				$mail->addAddress("".$data_config['email_reciever']."","Pastiche Management");  //tujuan email
				$mail->MsgHTML($message);
				$mail->Send();
	}

	public function duplicate_booking()
	{
		$data_duplicate 		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'bk_booking_id_pk','bookings');

		$kontroller				= $this->session->userdata('kontroller');
		
		$bk_event_name			= $data_duplicate['bk_event_name'];
		$bk_venue				= $data_duplicate['bk_venue'];
		$bk_date_of_event		= $data_duplicate['bk_date_of_event'];
		$bk_time_of_event		= $data_duplicate['bk_time_of_event'];
		$bk_date_of_booking		= date('Y-m-d');
		$bk_forecast			= $data_duplicate['bk_forecast'];
		$bk_price				= $data_duplicate['bk_price'];

		$bk_cost				= $data_duplicate['bk_cost'];
		$bk_profit				= $data_duplicate['bk_profit'];
		$bk_user_id_fk			= $data_duplicate['bk_user_id_fk'];
		$bk_status				= $data_duplicate['bk_status'];
		$bk_commission_only		= $data_duplicate['bk_commission_only'];
		$bk_contract_required	= $data_duplicate['bk_contract_required'];
		$bk_invoice_required	= $data_duplicate['bk_invoice_required'];
		$bk_website_enquiry		= $data_duplicate['bk_website_enquiry'];
		$bk_european_booking	= $data_duplicate['bk_european_booking'];
		$bk_quality_lead		= $data_duplicate['bk_quality_lead'];
		$bk_contact_id_fk		= $data_duplicate['bk_contact_id_fk'];
		$bk_po_heading			= $data_duplicate['bk_po_heading'];
		$bk_payment_terms		= $data_duplicate['bk_payment_terms'];
		$bk_lead_notes			= $data_duplicate['bk_lead_notes'];
		$bk_rota				= $data_duplicate['bk_rota'];
		$bk_notes				= $data_duplicate['bk_notes'];

			$data = array(		
				'bk_event_name' 		=> $bk_event_name,
				'bk_venue' 				=> $bk_venue,
				'bk_date_of_event'		=> $bk_date_of_event,
				'bk_time_of_event'		=> $bk_time_of_event,
				'bk_date_of_booking'	=> $bk_date_of_booking,
				'bk_forecast'			=> $bk_forecast,
				'bk_price'				=> $bk_price,
				'bk_cost'				=> $bk_cost,
				'bk_profit'				=> $bk_profit,
				'bk_user_id_fk'			=> $bk_user_id_fk,
				'bk_status'				=> $bk_status,
				'bk_commission_only'	=> $bk_commission_only,
				'bk_contract_required'	=> $bk_contract_required,
				'bk_invoice_required'	=> $bk_invoice_required,
				'bk_website_enquiry'	=> $bk_website_enquiry,
				'bk_european_booking'	=> $bk_european_booking,
				'bk_quality_lead'		=> $bk_quality_lead,
				'bk_contact_id_fk'		=> $bk_contact_id_fk,
				'bk_po_heading'			=> $bk_po_heading,
				'bk_payment_terms'		=> $bk_payment_terms,
				'bk_lead_notes'			=> $bk_lead_notes,
				'bk_rota'				=> $bk_rota,
				'bk_notes'				=> $bk_notes,
				);



			
			$this->web_app_model->insertData($data,'bookings');

			$last_id = $this->web_app_model->getMaxOneData('bk_booking_id_pk','bookings');

			header('location:'.base_url().'index.php/'.$kontroller.'/bg_editBooking/'.$last_id['bk_booking_id_pk'].'?booking=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Booking Schedule has been added!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Booking Schedule has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");		


				// CONFIG ============ Wajib Ada
				$data_config			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
				// END CONFIG ======== Wajib Ada

				// NOTIFIKASI BY EMAIL

				require("vendor/PHPMailer-master/src/PHPMailer.php");
				require("vendor/PHPMailer-master/src/SMTP.php");
				require("vendor/PHPMailer-master/src/Exception.php");
				require("vendor/PHPMailer-master/src/OAuth.php");
				    
				$message = '
				    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				</head>

				<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
				<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
				NOTICE : BOOKING HAS BEEN ADDED!    <br><br>
				        <div style="float:left; width:150px; margin-bottom:5px;">Event Name  :</div>
				        <div style="float:left;"><strong>'.$bk_event_name.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Date of event  :</div>
				        <div style="float:left;"><strong>'.$bk_date_of_event.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Date of booking:</div>
				        <div style="float:left;"><strong>'.$bk_date_of_booking.'</strong></div>
				        <div style="clear:both"></div>

				        <div style="float:left; width:150px; margin-bottom:5px;">===============</div>
				        <div style="float:left;"><strong>===============</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">-</div>
				        <div style="float:left;"><strong>-</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Status:</div>
				        <div style="float:left;"><strong>'.$bk_status.'</strong></div>
				        <div style="clear:both"></div>
				 <td><tr></table>
				 <br><br><br><a href="'.base_url().'index.php"><b>==> Go to Diary Pastiche <==</b></a>
				</body>
				</html>';

				  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
				//$mail = new PHPMailer;
				$mail->IsSMTP();
				$mail->SMTPSecure = 'ssl';
				$mail->Host = "mail.pastichestudios.co.uk"; //host masing2 provider email
				$mail->SMTPDebug = 1;
				$mail->Port = 465;
				$mail->SMTPAuth = true;
				$mail->IsHTML(true);
				$mail->Username = "".$data_config['email_sender'].""; //user email yang sebelumnya anda buat
				$mail->Password = "".$data_config['email_sender_pass'].""; //password email yang sebelumnya anda buat
				$mail->SetFrom("".$data_config['email_sender']."","Pastiche Diary"); //set email pengirim
				$mail->Subject = "Pastiche Diary - Booking Has Been Added!"; //subyek email
				$mail->addAddress("".$data_config['email_reciever']."","Pastiche Management");  //tujuan email
				$mail->MsgHTML($message);
				$mail->Send();
	}

	public function editBooking()
	{
		$kontroller 			= $this->session->userdata('kontroller');
		
		$bk_booking_id_pk		= $this->input->post('bk_booking_id_pk');
		$bk_event_name			= $this->input->post('bk_event_name');
		$bk_venue				= $this->input->post('bk_venue');
		$bk_date_of_event		= $this->input->post('bk_date_of_event');
		$bk_time_of_event		= $this->input->post('bk_time_of_event');
		$bk_date_of_booking		= $this->input->post('bk_date_of_booking');
		$bk_forecast			= $this->input->post('bk_forecast');
		$bk_price				= $this->input->post('bk_price');

		$bk_cost				= $this->input->post('bk_cost');
		$bk_profit				= $this->input->post('bk_profit');
		$bk_user_id_fk			= $this->input->post('bk_user_id_fk');
		$bk_status				= $this->input->post('bk_status');
		$bk_commission_only		= $this->input->post('bk_commission_only');
		$bk_contract_required	= $this->input->post('bk_contract_required');
		$bk_invoice_required	= $this->input->post('bk_invoice_required');
		$bk_website_enquiry		= $this->input->post('bk_website_enquiry');
		$bk_european_booking	= $this->input->post('bk_european_booking');
		$bk_quality_lead		= $this->input->post('bk_quality_lead');
		$bk_contact_id_fk		= $this->input->post('bk_contact_id_fk');
		$bk_po_heading			= $this->input->post('bk_po_heading');
		$bk_payment_terms		= $this->input->post('bk_payment_terms');
		$bk_lead_notes			= $this->input->post('bk_lead_notes');
		$bk_rota				= $this->input->post('bk_rota');
		$bk_notes				= $this->input->post('bk_notes');

		$data = array(		
				'bk_event_name' 		=> $bk_event_name,
				'bk_venue' 				=> $bk_venue,
				'bk_date_of_event'		=> $bk_date_of_event,
				'bk_time_of_event'		=> $bk_time_of_event,
				'bk_date_of_booking'	=> $bk_date_of_booking,
				'bk_forecast'			=> $bk_forecast,
				'bk_price'				=> $bk_price,
				'bk_cost'				=> $bk_cost,
				'bk_profit'				=> $bk_profit,
				'bk_user_id_fk'			=> $bk_user_id_fk,
				'bk_status'				=> $bk_status,
				'bk_commission_only'	=> $bk_commission_only,
				'bk_contract_required'	=> $bk_contract_required,
				'bk_invoice_required'	=> $bk_invoice_required,
				'bk_website_enquiry'	=> $bk_website_enquiry,
				'bk_european_booking'	=> $bk_european_booking,
				'bk_quality_lead'		=> $bk_quality_lead,
				'bk_contact_id_fk'		=> $bk_contact_id_fk,
				'bk_po_heading'			=> $bk_po_heading,
				'bk_payment_terms'		=> $bk_payment_terms,
				'bk_lead_notes'			=> $bk_lead_notes,
				'bk_rota'				=> $bk_rota,
				'bk_notes'				=> $bk_notes,
				);

		$where = array(		
			'bk_booking_id_pk' 			=> $bk_booking_id_pk,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'bookings');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_booking?booking=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Booking Schedule has been edited!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Success!!',
												                text:  'Booking Schedule has been edited!',
												                type: 'success',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  

												    var delayInMilliseconds = 2000; //2 second

													setTimeout(function() {
													  jQuery(document).ready(function() {
												            close();
												        });
													}, delayInMilliseconds);
													
											    </script>
											    ");


		// CONFIG ============ Wajib Ada
		$data_config			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
		// END CONFIG ======== Wajib Ada

		// NOTIFIKASI BY EMAIL

		require("vendor/PHPMailer-master/src/PHPMailer.php");
		require("vendor/PHPMailer-master/src/SMTP.php");
		require("vendor/PHPMailer-master/src/Exception.php");
		require("vendor/PHPMailer-master/src/OAuth.php");
		    
		$message = '
		    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		</head>

		<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
		<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
		NOTICE : BOOKING HAS BEEN UPDATED!    <br><br>
		        <div style="float:left; width:150px; margin-bottom:5px;">Event Name  :</div>
		        <div style="float:left;"><strong>'.$bk_event_name.'</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Date of event  :</div>
		        <div style="float:left;"><strong>'.$bk_date_of_event.'</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Date of booking:</div>
		        <div style="float:left;"><strong>'.$bk_date_of_booking.'</strong></div>
		        <div style="clear:both"></div>

		        <div style="float:left; width:150px; margin-bottom:5px;">===============</div>
		        <div style="float:left;"><strong>===============</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">-</div>
		        <div style="float:left;"><strong>-</strong></div>
		        <div style="clear:both"></div>
		        <div style="float:left; width:150px; margin-bottom:5px;">Status:</div>
		        <div style="float:left;"><strong>'.$bk_status.'</strong></div>
		        <div style="clear:both"></div>
		 <td><tr></table>
		 <br><br><br><a href="'.base_url().'index.php"><b>==> Go to Diary Pastiche <==</b></a>
		</body>
		</html>';

		  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
		//$mail = new PHPMailer;
		$mail->IsSMTP();
		$mail->SMTPSecure = 'ssl';
		$mail->Host = "mail.pastichestudios.co.uk"; //host masing2 provider email
		$mail->SMTPDebug = 1;
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->IsHTML(true);
		$mail->Username = "".$data_config['email_sender'].""; //user email yang sebelumnya anda buat
		$mail->Password = "".$data_config['email_sender_pass'].""; //password email yang sebelumnya anda buat
		$mail->SetFrom("".$data_config['email_sender']."","Pastiche Diary"); //set email pengirim
		$mail->Subject = "Pastiche Diary - Booking Has Been Updated!"; //subyek email
		$mail->addAddress("".$data_config['email_reciever']."","Pastiche Management");  //tujuan email
		$mail->MsgHTML($message);
		$mail->Send();
	}	

	public function hapusBooking()
	{

		$kontroller 	= $this->session->userdata('kontroller');
		$id 			= $this->uri->segment(3);
		$hapus 			= array('bk_booking_id_pk'=>$id);
		$hapus2 		= array('ab_booking_id_fk'=>$id);


		$this->web_app_model->deleteData('bookings',$hapus);
		$this->web_app_model->deleteData('artiste_bookings',$hapus2);
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_booking/?booking=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Booking Schedule been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Booking Schedule has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}


// SECTION BOOKING


// PASSWORD

	public function bg_password()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Administrator')
		{
			
			// CONFIG ============ Wajib Ada
			$bc['data_config']			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			//$bc['data_booking'] 		= $this->web_app_model->get_booking_1('bk_booking_id_pk',$this->uri->segment(3));
			//$bc['data_users']			= $this->web_app_model->get2WhereAllItemOrder('2','usr_security_profile_id_fk','1','usr_active','users','usr_user_id_pk','DESC');
			//$bc['data_client']			= $this->web_app_model->getWhereAllItem('1','co_active','contacts');
			$bc['usr_first_name'] 	= $this->session->userdata('usr_first_name');
			$bc['usr_last_name'] 		= $this->session->userdata('usr_last_name');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			$bc['atas'] 				= $this->load->view('page/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('page/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('page/bio',$bc,true);	
			$bc['menu_atasbawah'] 		= $this->load->view('page/menu_atasbawah',$bc,true);

			//$bc['modalEditUsers'] 		= $this->load->view('page/modalEditUsers',$bc,true);
			//$bc['modalTambahBooking'] 	= $this->load->view('page/modalTambahBooking',$bc,true);
			$this->load->view('page/bg_password',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function update_password()
	{

		$get_old_password 		= $this->session->userdata('users_password');
		$kontroller 			= $this->session->userdata('kontroller');
		$usr_username 		= $this->session->userdata('usr_username');
	
		$old_password			= md5($this->input->post('old_password'));
		$new_password			= $this->input->post('new_password');
		$new_password_confirm	= $this->input->post('new_password_confirm');

		if($old_password == $get_old_password)
		{
			if($new_password == $new_password_confirm)
			{
				$data = array(		
				'usr_password' 	=> md5($new_password_confirm),
				);

				$where = array(		
					'usr_username' 		=> $usr_username,
					);
				
				$this->web_app_model->updateDataWhere($where,$data,'users');
				header('location:'.base_url().'index.php/'.$kontroller.'/bg_password?password=1');
				$this->session->set_flashdata("logout","<meta http-equiv = 'refresh' content = '5;url = ".base_url()."index.php/welcome/logout'/>");
				$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
														<button type='button' class='close' data-dismiss='alert'>
															<i class='icon-remove'></i>
														</button>
				
														<p>
															<strong>
																<i class='icon-ok'></i>
																Success! - 
															</strong>
															New Password has been updated!
														</p>
													</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
													     setTimeout(function () { 
													     swal({
													                title: 'Success!!',
													                text:  'New password has been updated!, in a few seconds the system will log out automatically for security',
													                type: 'success',
													                timer: 30000,
													            });  
													     },10);  
													    </script>
													    ");
			}
			else
			{
				header('location:'.base_url().'index.php/'.$kontroller.'/bg_password?password=1');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
														<button type='button' class='close' data-dismiss='alert'>
															<i class='icon-remove'></i>
														</button>
				
														<p>
															<strong>
																<i class='icon-ok'></i>
																Ops! - 
															</strong>
															Password confirmation is wrong!
														</p>
													</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
													     setTimeout(function () { 
													     swal({
													                title: 'Ops!!',
													                text:  'Password confirmation is wrong!',
													                type: 'warning',
													                timer: 3000,
													                showConfirmButton: true
													            });  
													     },10);  
													    </script>
													    ");
			}
		}
		else
		{
			header('location:'.base_url().'index.php/'.$kontroller.'/bg_password?password=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Ops! - 
														</strong>
														Old password is wrong!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Ops!!',
												                text:  'Old password is wrong!',
												                type: 'warning',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
		}
	}

	public function resetPassword()
	{


		$string 			= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-";
		$reset 				= substr(str_shuffle($string), 0,8);
		$kontroller			= $this->session->userdata('kontroller');
		
		$usr_username   	= $this->uri->segment(3);

		$data_user			= $this->web_app_model->getWhereOneItem($usr_username,'usr_username','users');

		$data = array(		
		'usr_password' 	=> md5($reset),
		);

		$where = array(		
			'usr_username' 		=> $usr_username,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'users');
		header('location:'.base_url().'index.php/'.$kontroller.'/bg_users?users=1');
		$this->session->set_flashdata("logout","<meta http-equiv = 'refresh' content = '5;url = ".base_url()."index.php/welcome/logout'/>");
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Password has been reset and has been sent to user email!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Password has been reset and has been sent to user email!',
											                type: 'success',
											                timer: 30000,
											            });  
											     },10);  
											    </script>
											    ");

		// CONFIG ============ Wajib Ada
				$data_config			= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
				// END CONFIG ======== Wajib Ada

				// NOTIFIKASI BY EMAIL

				require("vendor/PHPMailer-master/src/PHPMailer.php");
				require("vendor/PHPMailer-master/src/SMTP.php");
				require("vendor/PHPMailer-master/src/Exception.php");
				require("vendor/PHPMailer-master/src/OAuth.php");
				    
				$message = '
				    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				</head>

				<body style="font-family:Verdana, Geneva, sans-serif;font-size:12px;">
				<table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:20px;border:dashed 1px #333;"><tr><td>
				Your account has been reset with the following details:    <br><br>
				        <div style="float:left; width:150px; margin-bottom:5px;">Username  :</div>
				        <div style="float:left;"><strong>'.$usr_username.'</strong></div>
				        <div style="clear:both"></div>
				        <div style="float:left; width:150px; margin-bottom:5px;">Password:</div>
				        <div style="float:left;"><strong>'.$reset.'</strong></div>
				        <div style="clear:both"></div><br><br>
				 Please login and change your password for the security of your account
				 <td><tr></table>
				 <br><br><br><a href="'.base_url().'index.php"><b>==> Login to Diary Pastiche <==</b></a>
				</body>
				</html>';

				  $mail = new PHPMailer\PHPMailer\PHPMailer(); 
				//$mail = new PHPMailer;
				$mail->IsSMTP();
				$mail->SMTPSecure = 'ssl';
				$mail->Host = "mail.pastichestudios.co.uk"; //host masing2 provider email
				$mail->SMTPDebug = 1;
				$mail->Port = 465;
				$mail->SMTPAuth = true;
				$mail->IsHTML(true);
				$mail->Username = "".$data_config['email_sender'].""; //user email yang sebelumnya anda buat
				$mail->Password = "".$data_config['email_sender_pass'].""; //password email yang sebelumnya anda buat
				$mail->SetFrom("".$data_config['email_sender']."","Pastiche Diary"); //set email pengirim
				$mail->Subject = "Pastiche Diary - Your account has been reset!"; //subyek email
				$mail->addAddress("".$data_user['usr_email_address']."","Pastiche Management");  //tujuan email
				$mail->MsgHTML($message);
				$mail->Send();
	}

// END PASSWORD
}
