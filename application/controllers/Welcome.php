<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * Web Developer at Moladev
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */
	public function _construct()
	{
		session_start();
	}

	public function __construct()
	{	
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('form_validation'));
		//$this->load->model('crud_model');
	}

	public function index()
	{

		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			// CONFIG ============
			$bc['data_config']	= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ========
			
			$this->load->view('tampilan_login', $bc);
		}
		else
		{
			if($stts == 'Administrator')
			{
				header('location:'.base_url().'index.php/admin');
			}
			if($stts == 'Sales')
			{
				header('location:'.base_url().'index.php/sales');
			}
		}
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/welcome');
		}
		else
		{
			$this->session->sess_destroy();
			header('location:'.base_url().'index.php/welcome');
		}
	}



	public function insert()
	{
		if ($this->input->is_ajax_request()) {

			$this->form_validation->set_rules('ab_artiste_id_fk', 'Artist', 'required');
			$this->form_validation->set_rules('ab_booking_id_fk', 'ID Booking', 'required');
			$this->form_validation->set_rules('ab_booking_fee', 'Fee Artist', '');
			$this->form_validation->set_rules('ab_paid', 'Paid Status', 'required');

			if ($this->form_validation->run() == FALSE) {
				$data = array('response' => "error", 'message' => validation_errors());
			} 
			else 
			{
				$ajax_data = $this->input->post();

				$booking = $this->input->post('ab_booking_id_fk');
                $artist  = $this->input->post('ab_artiste_id_fk');
                
                $cek     = $this->web_app_model->check_entry($booking, $artist);
                
                if($cek->num_rows() > 0)
                {
    				$data = array('response' => "error", 'message' => "Duplicate Entry");
                }
                else
                {
                    
                    if ($this->web_app_model->insert_entry($ajax_data))
    				{
    					$data = array('response' => "success", 'message' => "Data added successfully");
    				} 
    				else 
    				{
    					$data = array('response' => "error", 'message' => "failed");
    				}
                }
			}

			echo json_encode($data);
		} else {
			echo "'No direct script access allowed'";
		}
	}

	public function fetch()
	{
		if ($this->input->is_ajax_request()) {
			$posts = $this->web_app_model->get_entries();
			echo json_encode($posts);
		} else {
			echo "'No direct script access allowed'";
		}
	}

	public function fetch_edit()
	{
		if ($this->input->is_ajax_request()) {
			$posts = $this->web_app_model->get_entries_edit($this->uri->segment(3));
			echo json_encode($posts);
		} else {
			echo "'No direct script access allowed'";
		}
	}

	public function delete()
	{
		if ($this->input->is_ajax_request()) {

			$ab_no = $this->input->post('ab_no');

			if ($this->web_app_model->delete_entry($ab_no)) {
				$data = array('response' => "success",);
			} else {
				$data = array('response' => "error");
			}

			echo json_encode($data);
		}
	}

	 public function edit()
	{
		if ($this->input->is_ajax_request()) 
		{
			$this->input->post('ab_no');

			$ab_no = $this->input->post('ab_no');

			if ($post = $this->web_app_model->single_entry($ab_no)) {
				$data = array('response' => "success", 'post' => $post);
			} else {
				$data = array('response' => "error", 'message' => "failed");
			}

			echo json_encode($data);
		}
	}

	public function update()
	{
		if ($this->input->is_ajax_request()) {
			$this->form_validation->set_rules('ab_paid', 'Paid Status', 'required');

			if ($this->form_validation->run() == FALSE) {
				$data = array('response' => "error", 'message' => validation_errors());
			} else {
				$data['ab_no'] = $this->input->post('ab_no');
				$data['ab_paid'] = $this->input->post('ab_paid');
				$data['ab_booking_fee'] = $this->input->post('ab_booking_fee');

				if ($this->web_app_model->update_entry($data)) {
					$data = array('response' => "success", 'message' => "Data update successfully");
				} else {
					$data = array('response' => "error", 'message' => "failed");
				}
			}

			echo json_encode($data);
		} else {
			echo "'No direct script access allowed'";
		}
	}

}
